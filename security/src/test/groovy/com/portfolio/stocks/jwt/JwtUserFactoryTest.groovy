package com.portfolio.stocks.jwt

import com.portfolio.stocks.domain.User
import spock.lang.Specification

class JwtUserFactoryTest extends Specification {

    private JwtUserFactory cut = new JwtUserFactory()
    private User user = Stub(User.class)

    def "should create JwtUser"() {
        given:
        user.getUsername() >> "username"
        user.getEmail() >> "email"
        user.getPassword() >> "password"
        user.getAuthorities() >> Arrays.asList("authority")
        user.getEnabled() >> true

        when:
        JwtUser jwtUser = cut.create(user)

        then:
        jwtUser.getUsername() == "username"
        jwtUser.getEmail() == "email"
        jwtUser.getPassword() == "password"
        jwtUser.isEnabled()
    }
}
