package com.portfolio.stocks.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import com.fasterxml.jackson.databind.ObjectMapper
import com.portfolio.stocks.config.RoutingConfiguration
import com.portfolio.stocks.domain.User
import com.portfolio.stocks.domain.UserDto
import com.portfolio.stocks.domain.VerificationToken
import com.portfolio.stocks.jwt.JwtAuthenticationRequest
import com.portfolio.stocks.jwt.JwtTokenUtil
import com.portfolio.stocks.service.RoutingService
import com.portfolio.stocks.service.UserService
import org.bson.types.ObjectId
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Ignore
import spock.lang.Specification

class AuthenticationRestControllerTest extends Specification {

    MockMvc mockMvc
    AuthenticationManager authenticationManager = Mock()
    JwtTokenUtil jwtTokenUtil = Mock()
    UserDetailsService userDetailsService = Mock()
    UserDetails userDetails = Mock()
    ApplicationEventPublisher eventPublisher = Mock()
    RoutingService routingService = Mock()
    RoutingConfiguration routingConfig = Mock()

    UserService userService = Mock()


    def setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(
                new AuthenticationRestController(
                        authenticationManager,
                        jwtTokenUtil,
                        userService,
                        userDetailsService,
                        eventPublisher,
                        routingService,
                        routingConfig
                ))
                .build()
    }

    def "should create AuthenticationToken"() {
        given:
        JwtAuthenticationRequest jwtAuthenticationRequest =
                new JwtAuthenticationRequest("testUser", "password1")

        userDetailsService.loadUserByUsername(jwtAuthenticationRequest.getUsername()) >> userDetails
        jwtTokenUtil.generateToken(userDetails) >> "token"

        when:
        def results = mockMvc.perform(post('/user/login')
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(jwtAuthenticationRequest)))
                .andReturn().response

        then:
        results.status == 200
    }

    @Ignore
    def "should register UserAccount"() {
        given:
        UserDto accountDto = Mock()
        accountDto.getUsername() >> 'user'
        accountDto.getPassword() >> 'password'
        accountDto.getMatchingPassword() >> 'password'
        accountDto.getEmail() >> 'test@test.com'

        def user = new User()
        def token = new VerificationToken('token', 1440)
        def id = new ObjectId(new Date(2018, 12, 12))
        user.set_id(id)
        user.setUsername('user')
        user.setPassword('password')
        user.setEmail('test@test.com')
        user.setEnabled(false)
        user.setVerificationToken(token)
        user.setLastPasswordResetDate(new Date())
        user.setAuthorities(Collections.singletonList())

        userService.registerNewUserAccount(accountDto) >> Optional.of(user)

        when:
        def response = mockMvc.perform(post('/user/register')
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andReturn().response

        then:
        response.status == 200
    }

}
