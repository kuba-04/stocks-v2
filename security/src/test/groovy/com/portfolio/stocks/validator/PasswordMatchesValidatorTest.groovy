package com.portfolio.stocks.validator

import com.portfolio.stocks.domain.UserDto
import com.portfolio.stocks.exceptions.PasswordsDontMatchException
import spock.lang.Specification

import javax.validation.ConstraintValidatorContext

class PasswordMatchesValidatorTest extends Specification {

    private PasswordMatchesValidator cut = new PasswordMatchesValidator()
    private ConstraintValidatorContext context = Stub(ConstraintValidatorContext.class)
    private UserDto user = Stub(UserDto.class)

    def "should accept matching password"() {
        given:
        user.getPassword() >> "password"
        user.getMatchingPassword() >> "password"

        when:
        boolean result = cut.isValid(user, context)

        then:
        result
    }

    def "should not accept wrong matching password"() {
        given:
        user.getPassword() >> "password"
        user.getMatchingPassword() >> "Password"

        when:
        cut.isValid(user, context)

        then:
        PasswordsDontMatchException ex = thrown()
        ex.getMessage() == "Passwords don't match!"
    }
}
