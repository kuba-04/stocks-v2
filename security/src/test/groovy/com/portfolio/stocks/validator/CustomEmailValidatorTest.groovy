package com.portfolio.stocks.validator

import com.portfolio.stocks.exceptions.InvalidEmailFormatException
import spock.lang.Specification

import javax.validation.ConstraintValidatorContext

class CustomEmailValidatorTest extends Specification {

    private CustomEmailValidator cut = new CustomEmailValidator()
    private ConstraintValidatorContext context = Stub(ConstraintValidatorContext.class)

    def "should accept correct email"() {
        given:
        String email = "test@test.com"

        when:
        boolean result = cut.isValid(email, context)

        then:
        result
    }

    def "should not accept email with no '@'"() {
        given:
        String email = "test.com"

        when:
        cut.isValid(email, context)

        then:
        InvalidEmailFormatException ex = thrown()
        ex.getMessage() == "Invalid format: " + email
    }

    def "should not accept email with no '.com'"() {
        given:
        String email = "test@test"

        when:
        cut.isValid(email, context)

        then:
        InvalidEmailFormatException ex = thrown()
        ex.getMessage() == "Invalid format: " + email
    }

    def "should not accept null email"() {
        given:
        String email = null

        when:
        cut.isValid(email, context)

        then:
        IllegalArgumentException ex = thrown()
        ex.getMessage() == "Email cannot be null!"
    }
}
