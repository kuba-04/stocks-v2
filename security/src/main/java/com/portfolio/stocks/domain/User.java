package com.portfolio.stocks.domain;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "users")
@Data
public class User {

    @Id
    private ObjectId _id;
    private String username;
    private String password;
    private String email;
    private Boolean enabled;
    private Date lastPasswordResetDate;
    private List<String> authorities;
    private VerificationToken verificationToken;
}