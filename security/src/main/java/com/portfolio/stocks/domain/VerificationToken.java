package com.portfolio.stocks.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
public class VerificationToken {

    @Getter
    private String token;
    @Getter
    private LocalDateTime expiryDate;

    private LocalDateTime calculateExpiryDate(final int expiryTimeInMinutes) {
        return LocalDateTime.now().plusMinutes(expiryTimeInMinutes);
    }

    public VerificationToken(final String token, final int expiry) {
        this.token = token;
        this.expiryDate = calculateExpiryDate(expiry);
    }
}
