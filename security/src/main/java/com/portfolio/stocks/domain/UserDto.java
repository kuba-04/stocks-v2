package com.portfolio.stocks.domain;

import com.portfolio.stocks.annotations.ValidEmail;
import lombok.Getter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
public class UserDto extends UserWithValidPasswordDto {

    @Id
    private ObjectId _id;
    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
    private VerificationToken verificationToken;

}