package com.portfolio.stocks.domain;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "authorities")
@Getter
public class Authority {

    @Id
    private Long id;
    private AuthorityName name;
    private List<User> users;
}