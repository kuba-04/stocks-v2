package com.portfolio.stocks.domain;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class EmailDto implements Serializable {
    private String email;
}
