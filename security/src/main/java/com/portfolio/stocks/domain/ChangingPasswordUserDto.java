package com.portfolio.stocks.domain;

import lombok.Getter;

@Getter
public class ChangingPasswordUserDto extends UserWithValidPasswordDto {

    private String tempPassword;

}