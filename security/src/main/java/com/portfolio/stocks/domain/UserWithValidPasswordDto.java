package com.portfolio.stocks.domain;

import com.portfolio.stocks.annotations.PasswordMatches;
import lombok.Getter;

@Getter
@PasswordMatches
public class UserWithValidPasswordDto {

    private String username;
    private String password;
    private String matchingPassword;

}
