package com.portfolio.stocks.domain;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}