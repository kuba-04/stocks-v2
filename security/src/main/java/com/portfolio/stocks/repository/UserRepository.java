package com.portfolio.stocks.repository;

import com.portfolio.stocks.domain.User;
import org.springframework.data.mongodb.repository.DeleteQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends MongoRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);

    @Query(value = "{ 'verificationToken.token' : ?0 }")
    User findByVerificationToken(String token);

    @DeleteQuery("{ $and : [ {enabled : false}, {'verificationToken.expiryDate' : { $lt: ?0 }} ] }")
    List<User> deleteAllByExpiryDate(Date now);

    void deleteByUsername(String username);
}
