package com.portfolio.stocks.registration.verification.email;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
class MailCreatorService {

    private final TemplateEngine templateEngine;

    String buildEmail(final String message, final String link) {
        Context context = new Context();
        context.setVariable("message", message);
        context.setVariable("link", link);
        return templateEngine.process("mail-template", context);
    }
}
