package com.portfolio.stocks.registration.verification.events;

import com.portfolio.stocks.domain.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnPasswordResetEvent extends ApplicationEvent {

    private User user;

    public OnPasswordResetEvent(
            final User user) {
        super(user);
        this.user = user;
    }
}
