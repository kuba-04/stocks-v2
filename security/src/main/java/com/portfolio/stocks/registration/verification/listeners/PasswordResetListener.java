package com.portfolio.stocks.registration.verification.listeners;

import com.portfolio.stocks.config.RegistrationConfiguration;
import com.portfolio.stocks.config.RoutingConfiguration;
import com.portfolio.stocks.domain.User;
import com.portfolio.stocks.registration.verification.email.MailService;
import com.portfolio.stocks.registration.verification.events.OnPasswordResetEvent;
import com.portfolio.stocks.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class PasswordResetListener implements ApplicationListener<OnPasswordResetEvent> {

    private static final int PASSWORD_LENGTH = 8;

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final MailService mailService;
    private final RegistrationConfiguration registrationConfig;
    private final RoutingConfiguration routingConfig;

    @Override
    public void onApplicationEvent(final OnPasswordResetEvent event) {
        this.resetPassword(event);
    }

    private void resetPassword(final OnPasswordResetEvent event) {
        User user = event.getUser();
        String newPassword = UUID.randomUUID().toString().substring(0, PASSWORD_LENGTH - 1);
        String newPasswordEncoded = passwordEncoder.encode(newPassword);
        user.setPassword(newPasswordEncoded);
        userService.saveUser(user);
        log.info("A new password was assigned to user " + user.getUsername() + ": " + newPassword);
        String link = routingConfig.getFrontendHost() + routingConfig.getChangePasswordEndpoint();
        try {
            mailService.send(user, registrationConfig.getPasswordResetMailSubject(), newPassword, link);
            log.info("Email has been sent");
        } catch (MailException e) {
            log.error("Failed to process email sending: ", e.getMessage(), e);
        }
    }
}
