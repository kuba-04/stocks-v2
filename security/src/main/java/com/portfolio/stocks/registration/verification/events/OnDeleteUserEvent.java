package com.portfolio.stocks.registration.verification.events;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnDeleteUserEvent extends ApplicationEvent {

    private String username;

    public OnDeleteUserEvent(
            final String username) {
        super(username);
        this.username = username;
    }
}
