package com.portfolio.stocks.registration.verification.email;

import com.portfolio.stocks.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender mailSender;
    private final MailCreatorService mailCreatorService;

    public void send(final User user, final String subject, final String text, final String link) {
        mailSender.send(composeMail(user, subject, text, link));
    }

    private MimeMessagePreparator composeMail(final User user, final String subject, final String text, final String link) {
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(user.getEmail());
            messageHelper.setSubject(subject);
            messageHelper.setText(mailCreatorService.buildEmail(text, link), true);
        };
    }
}
