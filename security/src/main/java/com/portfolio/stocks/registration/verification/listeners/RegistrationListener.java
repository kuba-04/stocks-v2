package com.portfolio.stocks.registration.verification.listeners;

import com.portfolio.stocks.config.RegistrationConfiguration;
import com.portfolio.stocks.config.RoutingConfiguration;
import com.portfolio.stocks.domain.User;
import com.portfolio.stocks.registration.verification.email.MailService;
import com.portfolio.stocks.registration.verification.events.OnRegistrationCompleteEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final MailService mailService;
    private final RegistrationConfiguration registrationConfig;
    private final RoutingConfiguration routingConfig;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = user.getVerificationToken().getToken();
        String link = routingConfig.getBackendHost()
                + routingConfig.getBackendRegistrationConfirmEndpoint()
                + token;
        try {
            mailService.send(user, registrationConfig.getWelcomeMailSubject(), "", link);
            log.info("Email has been sent");
        } catch (MailException e) {
            log.error("Failed to process email sending: ", e.getMessage(), e);
        }
    }
}
