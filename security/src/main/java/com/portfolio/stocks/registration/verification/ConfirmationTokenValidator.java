package com.portfolio.stocks.registration.verification;

import com.portfolio.stocks.domain.VerificationToken;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ConfirmationTokenValidator {

    public boolean validateToken(final VerificationToken token) {
        return token != null && !LocalDateTime.now().isAfter(token.getExpiryDate());
    }
}
