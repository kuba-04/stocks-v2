package com.portfolio.stocks.service;

import com.portfolio.stocks.config.RegistrationConfiguration;
import com.portfolio.stocks.domain.ChangingPasswordUserDto;
import com.portfolio.stocks.domain.User;
import com.portfolio.stocks.domain.UserDto;
import com.portfolio.stocks.domain.UserWithValidPasswordDto;
import com.portfolio.stocks.domain.VerificationToken;
import com.portfolio.stocks.exceptions.EmailExistsException;
import com.portfolio.stocks.exceptions.UserNotFoundException;
import com.portfolio.stocks.exceptions.UsernameExistsException;
import com.portfolio.stocks.registration.verification.ConfirmationTokenValidator;
import com.portfolio.stocks.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationTokenValidator tokenValidator;
    private final RegistrationConfiguration registrationConfig;

    @Transactional
    @Override
    public Optional<User> registerNewUserAccount(final UserDto accountDto) {
        validateIfNoExists(accountDto);

        User user = new User();
        user.setUsername(accountDto.getUsername());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setAuthorities(Collections.singletonList("ROLE_USER"));
        user.setEnabled(false);
        user.setVerificationToken(new VerificationToken(UUID.randomUUID().toString(), registrationConfig.getTokenExpiration()));

        log.info("A new user was registered: " + user.getUsername() + ", " + user.getEmail());
        return Optional.of(userRepository.save(user));
    }

    @Transactional
    public Optional<User> changeUserPassword(final ChangingPasswordUserDto accountDto) {
        validateIfExists(accountDto);

        User user = userRepository.findByUsername(accountDto.getUsername());
        if (!passwordEncoder.matches(accountDto.getTempPassword(), user.getPassword())) {
            return Optional.empty();
        }
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));

        log.info("Password was changed for: " + user.getUsername());
        return Optional.of(userRepository.save(user));
    }

    @Transactional
    public Optional<User> deleteUserAccount(final String username) {
        validateIfExists(username);

        userRepository.deleteByUsername(username);
        return Optional.empty();
    }

    public Optional<User> enableUserIfTokenVerified(final String token) {
        User user = getUser(token)
                .orElseThrow(() -> new UserNotFoundException("User with this token was not found."));

        if (tokenValidator.validateToken(user.getVerificationToken())) {
            return enableUser(user);
        }
        return Optional.empty();
    }

    @Transactional
    private Optional<User> enableUser(final User user) {
        user.setEnabled(true);
        userRepository.save(user);
        log.info("A new user " + user.getUsername() + " was just enabled!");
        return Optional.of(userRepository.save(user));
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return Optional.ofNullable(userRepository.findByEmail(email));
    }

    @Override
    public Optional<User> saveUser(final User user) {
        return Optional.of(userRepository.save(user));
    }

    @Override
    public Optional<User> getUser(final String token) {
        return Optional.ofNullable(userRepository.findByVerificationToken(token));
    }

    private void validateIfNoExists(final UserDto accountDto) {
        if (usernameExist(accountDto.getUsername())) {
            throw new UsernameExistsException("There is an account with that username: "
                    + accountDto.getUsername());
        }

        if (emailExist(accountDto.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: "
                    + accountDto.getEmail());
        }
    }

    private void validateIfExists(final UserWithValidPasswordDto accountDto) {
        validateIfExists(accountDto.getUsername());
    }

    private void validateIfExists(final String username) {
        if (!usernameExist(username)) {
            throw new UsernameExistsException("There is no account with that username: "
                    + username);
        }
    }

    private boolean usernameExist(final String username) {
        User user = userRepository.findByUsername(username);
        return user != null;
    }

    private boolean emailExist(final String email) {
        User user = userRepository.findByEmail(email);
        return user != null;
    }

}
