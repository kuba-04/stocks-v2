package com.portfolio.stocks.service;

import com.portfolio.stocks.config.RoutingConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;

@RequiredArgsConstructor
@Service
public class RoutingService {

    private final RoutingConfiguration routingConfig;

    /**
     * send to a specific URL in the front end.
     */
    public HttpHeaders getHttpHeaders(String endpoint) throws URISyntaxException {
        URI angular = new URI(routingConfig.getFrontendHost() + endpoint);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(angular);
        return httpHeaders;
    }
}
