package com.portfolio.stocks.service;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable {

    private String token;

    public JwtAuthenticationResponse(final String token) {
        this.token = token;
    }

    public JwtAuthenticationResponse() {}

    public String getToken() {
        return this.token;
    }

    public String getUserDisabled() {
        return "User disabled";
    }

    public String getBadCredentials() {
        return "Bad credentials";
    }

    public String getTokenExpired() {
        return "Token has expired. Please register again.";
    }

}
