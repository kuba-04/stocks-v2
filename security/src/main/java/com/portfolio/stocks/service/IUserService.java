package com.portfolio.stocks.service;

import com.portfolio.stocks.domain.User;
import com.portfolio.stocks.domain.UserDto;
import com.portfolio.stocks.exceptions.EmailExistsException;

import java.util.Optional;

public interface IUserService {

    Optional<User> registerNewUserAccount(UserDto accountDto)
            throws EmailExistsException;

    Optional<User> getUser(String token);

    Optional<User> findByEmail(String email);

    Optional<User> saveUser(User user);
}
