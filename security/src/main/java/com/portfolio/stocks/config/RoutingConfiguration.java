package com.portfolio.stocks.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "routing")
public class RoutingConfiguration {

    private String backendHost;
    private String frontendHost;
    private String frontendRegistrationConfirmedEndpoint;
    private String backendRegistrationConfirmEndpoint;
    private String changePasswordEndpoint;

}
