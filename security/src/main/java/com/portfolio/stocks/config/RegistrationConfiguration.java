package com.portfolio.stocks.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "registration")
public class RegistrationConfiguration {

    private int tokenExpiration;
    private String cleanupScheduler;
    private String welcomeMailSubject;
    private String passwordResetMailSubject;

}
