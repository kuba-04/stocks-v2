package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class PasswordsDontMatchException extends RuntimeException {
    public PasswordsDontMatchException(final String message) {
        super(message);
        log.error("Passwords don't match!");
    }
}
