package com.portfolio.stocks.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class UsernameExistsException extends RuntimeException {
    public UsernameExistsException(final String message) {
        super(message);
    }
}
