package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidEmailFormatException extends RuntimeException {
    public InvalidEmailFormatException(final String message) {
        super(message);
        log.error("Email has invalid format!");
    }
}
