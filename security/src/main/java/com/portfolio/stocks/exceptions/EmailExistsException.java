package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.CONFLICT)
public class EmailExistsException extends RuntimeException {
    public EmailExistsException(final String message) {
        super(message);
        log.error("Email already exists!");
    }
}
