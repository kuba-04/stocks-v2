package com.portfolio.stocks.controller;

import com.portfolio.stocks.config.RoutingConfiguration;
import com.portfolio.stocks.domain.ChangingPasswordUserDto;
import com.portfolio.stocks.domain.EmailDto;
import com.portfolio.stocks.domain.User;
import com.portfolio.stocks.domain.UserDto;
import com.portfolio.stocks.exceptions.AuthenticationException;
import com.portfolio.stocks.jwt.JwtAuthenticationRequest;
import com.portfolio.stocks.jwt.JwtTokenUtil;
import com.portfolio.stocks.registration.verification.events.OnDeleteUserEvent;
import com.portfolio.stocks.registration.verification.events.OnPasswordResetEvent;
import com.portfolio.stocks.registration.verification.events.OnRegistrationCompleteEvent;
import com.portfolio.stocks.service.JwtAuthenticationResponse;
import com.portfolio.stocks.service.RoutingService;
import com.portfolio.stocks.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequiredArgsConstructor
public class AuthenticationRestController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;
    private final UserDetailsService userDetailsService;
    private final ApplicationEventPublisher eventPublisher;
    private final RoutingService routingService;
    private final RoutingConfiguration routingConfig;

    /**
     * Login endpoint. Handles login requests, deals with disabled user and bad credentials.
     *
     * @param authenticationRequest
     * @return token
     */
    @PostMapping(value = "/user/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody final JwtAuthenticationRequest authenticationRequest) {
        ResponseEntity<?> responseEntity =
                authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        if (responseEntity.getStatusCode().is4xxClientError()) {
            return responseEntity;
        } else {
            final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
            final String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtAuthenticationResponse(token));
        }
    }

    /**
     * Registration endpoint. Triggers the event to send the confirmation email to new user
     *
     * @param accountDto
     * @return status 201 if successfully registered
     */
    @PostMapping(value = "/user/register")
    public ResponseEntity<?> registerUserAccount(@RequestBody @Valid final UserDto accountDto) {
        Optional<User> registeredUser = userService.registerNewUserAccount(accountDto);
        if (registeredUser.isPresent()) {
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser.get()));
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/user/registrationConfirm")
    public ResponseEntity<?> confirmRegistration(@RequestParam("token") final String token) throws URISyntaxException {
        Optional<User> verifiedUser = userService.enableUserIfTokenVerified(token);
        return verifiedUser.isPresent()
                ? new ResponseEntity<>(
                routingService.getHttpHeaders(routingConfig.getFrontendRegistrationConfirmedEndpoint()),
                HttpStatus.SEE_OTHER)
                : ResponseEntity.badRequest().body(new JwtAuthenticationResponse().getTokenExpired());
    }

    @PostMapping(value = "/user/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestBody final EmailDto email) {
        Optional<User> user = userService.findByEmail(email.getEmail());
        if (user.isPresent()) {
            eventPublisher.publishEvent(new OnPasswordResetEvent(user.get()));
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(value = "/user/logout")
    public ResponseEntity<?> logout(final HttpServletRequest request, final HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
            return ResponseEntity.ok(HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(value = "/user/delete/{username}")
    public ResponseEntity<?> deleteAccount(@PathVariable final String username) {
        Optional<User> user = userService.deleteUserAccount(username);
        if (!user.isPresent()) {
            eventPublisher.publishEvent(new OnDeleteUserEvent(username));
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping(value = "/user/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody @Valid final ChangingPasswordUserDto accountDto) {
        Optional<User> updatedUser = userService.changeUserPassword(accountDto);
        return updatedUser.isPresent()
                ? ResponseEntity.ok(HttpStatus.OK)
                : ResponseEntity.badRequest().body(new JwtAuthenticationResponse().getTokenExpired());
    }

    /**
     * Authenticates the user. If something is wrong error message is logged and response prepared for client.
     *
     * @param username
     * @param password
     * @return responseEntity
     * @throws AuthenticationException
     */
    private ResponseEntity<?> authenticate(final String username, final String password) throws AuthenticationException {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (DisabledException e) {
            log.error("User is disabled!");
            return ResponseEntity.badRequest().body(new JwtAuthenticationResponse().getUserDisabled());
        } catch (BadCredentialsException e) {
            log.error("Bad credentials!");
            return ResponseEntity.badRequest().body(new JwtAuthenticationResponse().getBadCredentials());
        }
    }
}
