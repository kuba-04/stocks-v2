package com.portfolio.stocks.validator;

import com.portfolio.stocks.annotations.ValidEmail;
import com.portfolio.stocks.exceptions.InvalidEmailFormatException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomEmailValidator
        implements ConstraintValidator<ValidEmail, String> {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+\\.(.[A-Za-z0-9]+)*(.[A-Za-z]+)$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    @Override
    public void initialize(ValidEmail constraintAnnotation) {}

    @Override
    public boolean isValid(final String email, final ConstraintValidatorContext context) {
        return validateEmail(email);
    }

    private boolean validateEmail(final String email) {
        if (email == null) {
            throw new IllegalArgumentException("Email cannot be null!");
        }
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            throw new InvalidEmailFormatException("Invalid format: " + email);
        } else {
            return true;
        }
    }
}
