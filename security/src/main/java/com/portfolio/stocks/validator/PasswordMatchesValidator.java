package com.portfolio.stocks.validator;

import com.portfolio.stocks.annotations.PasswordMatches;
import com.portfolio.stocks.domain.UserWithValidPasswordDto;
import com.portfolio.stocks.exceptions.PasswordsDontMatchException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, UserWithValidPasswordDto> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(final UserWithValidPasswordDto user, final ConstraintValidatorContext context) {
        if (user.getPassword().equals(user.getMatchingPassword())) {
            return true;
        } else {
            throw new PasswordsDontMatchException("Passwords don't match!");
        }
    }
}
