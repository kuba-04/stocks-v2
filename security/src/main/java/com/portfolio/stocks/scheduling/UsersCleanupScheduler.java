package com.portfolio.stocks.scheduling;

import com.portfolio.stocks.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UsersCleanupScheduler {

    private final UserRepository userRepository;

    @Scheduled(cron = "${registration.cleanupScheduler}")
    public void purgeExpired() {
        Date now = Date.from(Instant.now());
        userRepository.deleteAllByExpiryDate(now)
                .forEach(user -> log.info("Deleting user " + user.getUsername() + ". Token has expired."));
    }
}
