package com.portfolio.stocks.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.portfolio.stocks.service.PortfolioService
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class PortfolioControllerTest extends Specification {

    MockMvc mockMvc
    def portfolioService = Mock(PortfolioService)

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(
                new PortfolioController(portfolioService))
                .build()
    }

    def "should get 1 portfolio"() {
        expect:
        mockMvc.perform(get('/v2/portfolios/testOwner')
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
    }

    def "should delete portfolio"() {
        expect:
        mockMvc.perform(delete('/v2/portfolios/testOwner/testPortfolio')
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
    }
}
