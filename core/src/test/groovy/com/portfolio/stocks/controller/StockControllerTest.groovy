package com.portfolio.stocks.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.fasterxml.jackson.databind.ObjectMapper
import com.portfolio.stocks.dtos.StockDto
import com.portfolio.stocks.service.StockService
import com.portfolio.stocks.service.sorting.SortingService
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class StockControllerTest extends Specification {

    MockMvc mockMvc
    def sortingService = Mock(SortingService)
    def stockService = Mock(StockService)

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(
                new StockController(sortingService, stockService))
                .build()
    }

    def "should get portfolio stocks"() {
        expect:
        mockMvc.perform(get('/v2/stocks/testOwner/testPortfolio')
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
    }

    def "should post add stock"() {
        setup:
        def stockDto = new StockDto()

        expect:
        mockMvc.perform(post('/v2/stocks/add')
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(stockDto)))
                .andExpect(status().isOk())
    }

    def "should post delete stock"() {
        setup:
        def stockDto = new StockDto()

        expect:
        mockMvc.perform(post('/v2/stocks/delete')
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(stockDto)))
                .andExpect(status().isOk())
    }
}
