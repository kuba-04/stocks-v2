package com.portfolio.stocks.controller

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.fasterxml.jackson.databind.ObjectMapper
import com.portfolio.stocks.dtos.SortingOrderDto
import com.portfolio.stocks.service.sorting.SortingService
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class SortingControllerTest extends Specification {

    MockMvc mockMvc
    def sortingService = Mock(SortingService)

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(
                new SortingController(sortingService))
                .build()
    }

    def "should post custom sort order"() {
        setup:
        def sortingOrderDto = Mock(SortingOrderDto)
        sortingOrderDto.getSorting() >> "sorting"
        sortingOrderDto.getPortfolio() >> "portfolio"

        expect:
        mockMvc.perform(post('/v2/sorting-service/custom')
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(sortingOrderDto)))
                .andExpect(status().isOk())
    }

    def "should post default sort order"() {
        setup:
        def sortingOrderDto = Mock(SortingOrderDto)
        sortingOrderDto.getPortfolio() >> "portfolio"

        expect:
        mockMvc.perform(post('/v2/sorting-service/default')
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(sortingOrderDto)))
                .andExpect(status().isOk())
    }
}
