package com.portfolio.stocks.service.sorting

import static com.portfolio.stocks.normalizer.ParamConstants.DIVIDEND_YIELD
import static com.portfolio.stocks.normalizer.ParamConstants.PRICE_TO_BOOK
import com.portfolio.stocks.normalizer.DataNormalizer
import com.portfolio.stocks.normalizer.NormalizedDataProvider
import com.portfolio.stocks.repository.InMemoryStockRepository
import com.portfolio.stocks.utils.TestConstants
import spock.lang.Specification

class SortingServiceTest extends Specification {

    def stockRepository = new InMemoryStockRepository()
    def normalizedDataProvider = new NormalizedDataProvider()
    def dataNormalizer = new DataNormalizer(normalizedDataProvider)
    def constants = new TestConstants()
    def sortingService = new SortingService(stockRepository, dataNormalizer)

    def "should get stocks with existing order"() {
        given: "stocks with P/B as existing order"
        def exxon = constants.getStock("XOM")
        def chevron = constants.getStock("CVX")
        def apple = constants.getStock("AAPL")
        def goldcorp = constants.getStock("GG")

        exxon.setPriceToBook(3)
        chevron.setPriceToBook(2)
        apple.setPriceToBook(4)
        goldcorp.setPriceToBook(1)

        exxon.setSortOrderList(Collections.singletonList(PRICE_TO_BOOK))
        chevron.setSortOrderList(Collections.singletonList(PRICE_TO_BOOK))
        apple.setSortOrderList(Collections.singletonList(PRICE_TO_BOOK))
        goldcorp.setSortOrderList(Collections.singletonList(PRICE_TO_BOOK))

        and: "other parameters are against book value ordering"
        exxon.setPeRatio(5)
        chevron.setPeRatio(15)
        apple.setPeRatio(1)
        goldcorp.setPeRatio(20)

        exxon.setDividendYield(1)
        chevron.setDividendYield(20)
        apple.setDividendYield(0)
        goldcorp.setDividendYield(0)

        stockRepository.saveAll(Arrays.asList(exxon, chevron, apple, goldcorp))

        when: "stocks are retrieved with existing ordering"
        def orderedStocks = sortingService.getStocksWithExistingOrder(constants.getOwner(), constants.getPortfolio())

        then: "goldcorp is first, chevron second, exxon third, apple last"
        orderedStocks.get(0).getTicker() == goldcorp.getTicker()
        orderedStocks.get(1).getTicker() == chevron.getTicker()
        orderedStocks.get(2).getTicker() == exxon.getTicker()
        orderedStocks.get(3).getTicker() == apple.getTicker()
    }

    def "should get stocks with default order"() {
        given: "stocks"
        def exxon = constants.getStock("XOM")
        def chevron = constants.getStock("CVX")
        def apple = constants.getStock("AAPL")
        def goldcorp = constants.getStock("GG")

        exxon.setPeRatio(3)
        chevron.setPeRatio(2)
        apple.setPeRatio(4)
        goldcorp.setPeRatio(1)

        stockRepository.saveAll(Arrays.asList(exxon, chevron, apple, goldcorp))

        when: "stocks are retrieved with default ordering"
        def orderedStocks = sortingService.getStocksWithDefaultOrder(constants.getOwner(), constants.getPortfolio())

        then: "goldcorp is first, chevron second, exxon third, apple last"
        orderedStocks.get(0).getTicker() == goldcorp.getTicker()
        orderedStocks.get(1).getTicker() == chevron.getTicker()
        orderedStocks.get(2).getTicker() == exxon.getTicker()
        orderedStocks.get(3).getTicker() == apple.getTicker()
    }

    def "should get stocks with custom order"() {
        given: "stocks"
        def exxon = constants.getStock("XOM")
        def chevron = constants.getStock("CVX")
        def apple = constants.getStock("AAPL")
        def goldcorp = constants.getStock("GG")

        exxon.setDividendYield(3)
        chevron.setDividendYield(2)
        apple.setDividendYield(4)
        goldcorp.setDividendYield(1)

        stockRepository.saveAll(Arrays.asList(exxon, chevron, apple, goldcorp))

        when: "stocks are retrieved with default ordering"
        def orderedStocks = sortingService.getStocksWithCustomOrder(
                constants.getOwner(), Collections.singletonList(DIVIDEND_YIELD), constants.getPortfolio())

        then: "apple is first, exxon second, chevron third, goldcorp last"
        orderedStocks.get(0).getTicker() == apple.getTicker()
        orderedStocks.get(1).getTicker() == exxon.getTicker()
        orderedStocks.get(2).getTicker() == chevron.getTicker()
        orderedStocks.get(3).getTicker() == goldcorp.getTicker()
    }
}
