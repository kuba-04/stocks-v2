package com.portfolio.stocks.repository;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.utils.TestConstants;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@NoArgsConstructor
public class InMemoryStockRepository implements StockRepository {

    public ConcurrentHashMap<Long, Stock> stockMap = new ConcurrentHashMap<>();

    @Override
    public List<Stock> findAllByOwner(String owner) {
        return stockMap.values().stream()
                .filter(stock -> stock.getOwner().equals(owner))
                .collect(Collectors.toList());
    }

    @Override
    public List<Stock> findAllByOwnerAndPortfolio(String owner, String portfolio) {
        return stockMap.values().stream()
                .filter(stock -> stock.getOwner().equals(owner))
                .filter(stock -> stock.getPortfolio().equals(portfolio))
                .collect(Collectors.toList());
    }

    @Override
    public List<Stock> findAllByOwnerAndPortfolioSorted(String owner, String portfolio) {
        return stockMap.values().stream()
                .filter(stock -> stock.getOwner().equals(owner))
                .filter(stock -> stock.getPortfolio().equals(portfolio))
                .sorted(Comparator.comparing(Stock::getSortOrder))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Stock> findByOwnerAndTicker(String owner, String ticker) {
        return stockMap.values().stream()
                .filter(stock -> stock.getOwner().equals(owner))
                .filter(stock -> stock.getTicker().equals(ticker))
                .findFirst();
    }

    @Override
    public void deleteByOwnerAndTicker(String owner, String ticker) {
        stockMap.entrySet().removeIf(entry -> entry.getValue().getOwner().equals(owner) &&
                entry.getValue().getTicker().equals(ticker));
    }

    @Override
    public void deleteAllByOwnerAndPortfolio(String owner, String portfolio) {
        stockMap.entrySet().removeIf(entry -> entry.getValue().getOwner().equals(owner) &&
                entry.getValue().getPortfolio().equals(portfolio));
    }

    @Override
    public <S extends Stock> S save(S entity) {
        delete(entity);
        Stock stock = (Stock) new TestConstants().stock(entity);
        stock.setId(String.valueOf(computeNextKey()));
        stockMap.put(computeNextKey(), stock);
        return (S) stock;
    }

    private long computeNextKey() {
        return stockMap.keySet().stream().max(Comparator.naturalOrder()).orElse(0L) + 1;
    }

    @Override
    public <S extends Stock> List<S> saveAll(Iterable<S> entities) {
        List<Stock> list = new ArrayList<>();
        entities.forEach(entity -> {
            entity.setId(String.valueOf(computeNextKey()));
            stockMap.put(computeNextKey(), entity);
            list.add(entity);
        });
        return (List<S>) list;
    }

    @Override
    public Optional<Stock> findById(Long id) {
        return stockMap.values().stream()
                .filter(stock -> stock.getId().equals(id.toString()))
                .findFirst();
    }

    @Override
    public boolean existsById(Long id) {
        return stockMap.values().stream()
                .anyMatch(stock -> stock.getId().equals(id.toString()));
    }

    @Override
    public List<Stock> findAll() {
        return new ArrayList<>(stockMap.values());
    }

    @Override
    public Iterable<Stock> findAllById(Iterable<Long> longs) {
        return Collections.emptyList();
    }

    @Override
    public long count() {
        return stockMap.mappingCount();
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Stock entity) {
        Optional<Map.Entry<Long, Stock>> stockEntry = stockMap.entrySet().stream()
                .filter(entry -> entry.getValue().getOwner().equals(entity.getOwner()) &&
                        entry.getValue().getTicker().equals(entity.getTicker()))
                .findFirst();
        stockEntry.ifPresent(longStockEntry -> stockMap.remove(longStockEntry.getKey()));
    }

    @Override
    public void deleteAll(Iterable<? extends Stock> entities) {
        stockMap.entrySet().removeAll((Collection<?>) entities);
    }

    @Override
    public void deleteAll() {
        stockMap.clear();
    }

    @Override
    public List<Stock> findAll(Sort sort) {
        return new ArrayList<>(stockMap.values());
    }

    @Override
    public Page<Stock> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Stock> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends Stock> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends Stock> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Stock> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Stock> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Stock> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Stock> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Stock> boolean exists(Example<S> example) {
        return false;
    }
}
