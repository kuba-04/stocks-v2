package com.portfolio.stocks.client.data.retriever

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.portfolio.stocks.client.IEXTradingClient
import com.portfolio.stocks.client.data.retriever.trend.calculator.DataRetrieverService
import spock.lang.Specification

import java.text.DecimalFormat

class DataRetrieverServiceTest extends Specification {

    private IEXTradingClient iexTradingClient = Mock()
    private ObjectMapper objectMapper = Mock()
    private DataRetrieverService cut = new DataRetrieverService(iexTradingClient, objectMapper)

    def "should retrieve price"() {
        given:
        ClassLoader classLoader = new DataRetrieverServiceTest().getClass().getClassLoader()
        URL file = new File(classLoader.getResource("xom-price.json").getFile()).toURL()
        JsonNode jsonNode = new ObjectMapper().readTree(file).get("price")

        and:
        iexTradingClient.getPriceUrl("XOM") >> file
        objectMapper.readTree(file) >> jsonNode

        when:
        def result = cut.retrievePrice("XOM")

        then:
        result == 80.2
    }

    def "should retrieve stats and descriptions"() {
        given:
        DecimalFormat df = new DecimalFormat(".####")
        ClassLoader classLoader = new DataRetrieverServiceTest().getClass().getClassLoader()
        URL file = new File(classLoader.getResource("xom-stats-desc.json").getFile()).toURL()
        JsonNode root = new ObjectMapper().readTree(file)

        and:
        iexTradingClient.getFinancialsDescriptionUrl("XOM") >> file
        objectMapper.readTree(file) >> root

        when:
        Map<String, Object> result = cut.retrieveStatsAndDescription("XOM")

        then:
        result.get("companyName") == "Exxon Mobil Corporation"
        result.get("industry") == "Oil & Gas - Integrated"
        result.get("sector") == "Energy"
        result.get("description") == "Exxon Mobil Corp is an integrated oil and gas company. It is engaged in exploration for, and production of, crude oil and natural gas. It is also engaged in manufacturing, transportation and sale of crude oil, natural gas and petroleum products."

        result.get("price") == 81.495
        result.get("latestEPS") == 4.63
        result.get("peRatio") == df.format(result.get("price") / result.get("latestEPS")).toDouble()
        result.get("priceToBook") == 1.81
        result.get("returnOnEquity") == 11.03
        result.get("returnOnAssets") == 5.76
        result.get("dividendYield") == 4.0908
        result.get("roe_0") == df.format(3950000000 / 187222000000).toDouble()
        result.get("roe_1") == df.format(4650000000 / 188195000000).toDouble()
        result.get("roe_2") == df.format(8380000000 / 187688000000).toDouble()
        result.get("roe_3") == df.format(3970000000 / 182276000000).toDouble()
        result.get("actualEPS_0") == 0.92
        result.get("actualEPS_1") == 1.09
        result.get("actualEPS_2") == 0.88
        result.get("actualEPS_3") == 0.93
        result.get("debtToEquity_0") == df.format(41220000000 / 187222000000).toDouble()
        result.get("debtToEquity_1") == df.format(40617000000 / 188195000000).toDouble()
        result.get("debtToEquity_2") == df.format(42336000000 / 187688000000).toDouble()
        result.get("debtToEquity_3") == df.format(40610000000 / 182276000000).toDouble()
    }

    def "should retrieve stats"() {
        given:
        DecimalFormat df = new DecimalFormat(".####")
        ClassLoader classLoader = new DataRetrieverServiceTest().getClass().getClassLoader()
        URL file = new File(classLoader.getResource("xom-stats.json").getFile()).toURL()
        JsonNode root = new ObjectMapper().readTree(file)

        and:
        iexTradingClient.getFinancialsUrl("XOM") >> file
        objectMapper.readTree(file) >> root

        when:
        Map<String, Object> result = cut.retrieveStats("XOM")

        then:
        result.get("price") == 81.495
        result.get("latestEPS") == 4.63
        result.get("peRatio") == df.format(result.get("price") / result.get("latestEPS")).toDouble()
        result.get("priceToBook") == 1.81
        result.get("returnOnEquity") == 11.03
        result.get("returnOnAssets") == 5.76
        result.get("dividendYield") == 4.0908
        result.get("roe_0") == df.format(3950000000 / 187222000000).toDouble()
        result.get("roe_1") == df.format(4650000000 / 188195000000).toDouble()
        result.get("roe_2") == df.format(8380000000 / 187688000000).toDouble()
        result.get("roe_3") == df.format(3970000000 / 182276000000).toDouble()
        result.get("actualEPS_0") == 0.92
        result.get("actualEPS_1") == 1.09
        result.get("actualEPS_2") == 0.88
        result.get("actualEPS_3") == 0.93
        result.get("debtToEquity_0") == df.format(41220000000 / 187222000000).toDouble()
        result.get("debtToEquity_1") == df.format(40617000000 / 188195000000).toDouble()
        result.get("debtToEquity_2") == df.format(42336000000 / 187688000000).toDouble()
        result.get("debtToEquity_3") == df.format(40610000000 / 182276000000).toDouble()
    }

}
