package com.portfolio.stocks.client.data.retriever.trend.calculator

import com.portfolio.stocks.client.data.retriever.trend.calculator.trend.calculator.TrendCalculator
import org.mockito.InjectMocks
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration
class TrendCalculatorTest extends Specification {

    @InjectMocks
    private TrendCalculator trendCalculator

    def shouldCalculateTrend() {
        given:
        def vals = [3d, 2d, 1d, 0d]

        when:
        double trend = trendCalculator.calculateTrend(vals as Double[])

        then:
        trend == 1d
    }

    def shouldReturnZeroForInvalidSlope() {
        given:
        def vals = [Double.NaN, Double.NaN]

        when:
        double trend = trendCalculator.calculateTrend(vals as Double[])

        then:
        trend == 0d
    }
}
