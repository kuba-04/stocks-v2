package com.portfolio.stocks.client

import com.portfolio.stocks.client.data.retriever.trend.calculator.DataRetrieverService
import com.portfolio.stocks.client.data.retriever.trend.calculator.trend.calculator.TrendCalculator
import com.portfolio.stocks.domain.Stock
import com.portfolio.stocks.validators.IEXValidator
import spock.lang.Specification

class IEXStockCreatorTest extends Specification {

    private DataRetrieverService dataRetrieverService = Mock()
    private IEXValidator validator = Mock()
    private TrendCalculator trendCalculator = Mock()
    private IEXStockCreator cut = new IEXStockCreator(dataRetrieverService, validator, trendCalculator)

    def "should create IEXStock"() {
        given:
        String owner = "testOwner"
        String portfolio = "testPortfolio"
        String ticker = "TST"
        HashMap statsMap = getTestMap()

        validator.validateIfAlreadyExists(owner, ticker) >> null
        dataRetrieverService.retrieveStatsAndDescription(ticker) >> statsMap
        trendCalculator.calculateTrend(0.8d, 0.9d, 1.0d, 1.1d) >> 0.0

        when:
        Stock stock = cut.createIEXStock(owner, portfolio, ticker)

        then:
        stock.getName() == "testName"

    }

    private static HashMap getTestMap() {
        def statsMap = new HashMap<>()
        statsMap.put("companyName", "testName")
        statsMap.put("industry", "testIndustry")
        statsMap.put("sector", "testSector")
        statsMap.put("description", "testDescription")
        statsMap.put("price", 0.1d)
        statsMap.put("latestEPS", 0.2d)
        statsMap.put("peRatio", 0.3d)
        statsMap.put("priceToBook", 0.4d)
        statsMap.put("returnOnEquity", 0.5d)
        statsMap.put("returnOnAssets", 0.6d)
        statsMap.put("dividendYield", 0.7d)
        statsMap.put("roe_0", 0.8d)
        statsMap.put("roe_1", 0.9d)
        statsMap.put("roe_2", 1.0d)
        statsMap.put("roe_3", 1.1d)
        statsMap.put("actualEPS_0", 1.2d)
        statsMap.put("actualEPS_1", 1.3d)
        statsMap.put("actualEPS_2", 1.4d)
        statsMap.put("actualEPS_3", 1.5d)
        statsMap.put("debtToEquity_0", 1.6d)
        statsMap.put("debtToEquity_1", 1.7d)
        statsMap.put("debtToEquity_2", 1.8d)
        statsMap.put("debtToEquity_3", 1.9d)
        statsMap
    }
}