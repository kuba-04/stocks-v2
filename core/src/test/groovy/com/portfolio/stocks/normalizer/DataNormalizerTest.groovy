package com.portfolio.stocks.normalizer

import com.portfolio.stocks.domain.Stock
import com.portfolio.stocks.utils.TestConstants
import spock.lang.Specification
import java.text.DecimalFormat

class DataNormalizerTest extends Specification {

    def normalizedDataProvider = new NormalizedDataProvider()
    def dataNormalizer = new DataNormalizer(normalizedDataProvider)
    def constants = new TestConstants()
    def df = new DecimalFormat(".####")

    def "should normalize stock"() {
        given:
        def exxon = constants.getPreNormalizedStock("XOM")
        def chevron = constants.getPreNormalizedStock("CVX")
        def apple = constants.getPreNormalizedStock("AAPL")
        def goldcorp = constants.getPreNormalizedStock("GG")

        exxon.setPriceToBook(1)
        chevron.setPriceToBook(2)
        apple.setPriceToBook(3)
        goldcorp.setPriceToBook(4)

        exxon.setLatestEPS(1)
        chevron.setLatestEPS(2)
        apple.setLatestEPS(3)
        goldcorp.setLatestEPS(4)

        exxon.setPeRatio(1)
        chevron.setPeRatio(2)
        apple.setPeRatio(3)
        goldcorp.setPeRatio(4)

        exxon.setReturnOnEquity(1)
        chevron.setReturnOnEquity(2)
        apple.setReturnOnEquity(3)
        goldcorp.setReturnOnEquity(4)

        exxon.setReturnOnAssets(1)
        chevron.setReturnOnAssets(2)
        apple.setReturnOnAssets(3)
        goldcorp.setReturnOnAssets(4)

        exxon.setDividendYield(1)
        chevron.setDividendYield(2)
        apple.setDividendYield(3)
        goldcorp.setDividendYield(4)

        exxon.setTrend_ROE(1)
        chevron.setTrend_ROE(2)
        apple.setTrend_ROE(3)
        goldcorp.setTrend_ROE(4)

        exxon.setTrend_EPS(1)
        chevron.setTrend_EPS(2)
        apple.setTrend_EPS(3)
        goldcorp.setTrend_EPS(4)

        exxon.setTrend_DebtToEquity(1)
        chevron.setTrend_DebtToEquity(2)
        apple.setTrend_DebtToEquity(3)
        goldcorp.setTrend_DebtToEquity(4)

        List<Stock> stocks = new ArrayList([exxon, chevron, apple, goldcorp])

        when:
        dataNormalizer.normalize(stocks)

        then:
        stocks.get(0).getN_priceToBook() == 1
        stocks.get(1).getN_priceToBook() == formatNumber(2 / 3)
        stocks.get(2).getN_priceToBook() == formatNumber(1 / 3)
        stocks.get(3).getN_priceToBook() == 0

        stocks.get(0).getN_latestEPS() == 0
        stocks.get(1).getN_latestEPS() == formatNumber(1 / 3)
        stocks.get(2).getN_latestEPS() == formatNumber(2 / 3)
        stocks.get(3).getN_latestEPS() == 1

        stocks.get(0).getN_peRatio() == 1
        stocks.get(1).getN_peRatio() == formatNumber(2 / 3)
        stocks.get(2).getN_peRatio() == formatNumber(1 / 3)
        stocks.get(3).getN_peRatio() == 0

        stocks.get(0).getN_returnOnEquity() == 0
        stocks.get(1).getN_returnOnEquity() == formatNumber(1 / 3)
        stocks.get(2).getN_returnOnEquity() == formatNumber(2 / 3)
        stocks.get(3).getN_returnOnEquity() == 1

        stocks.get(0).getN_returnOnAssets() == 0
        stocks.get(1).getN_returnOnAssets() == formatNumber(1 / 3)
        stocks.get(2).getN_returnOnAssets() == formatNumber(2 / 3)
        stocks.get(3).getN_returnOnAssets() == 1

        stocks.get(0).getN_dividendYield() == 0
        stocks.get(1).getN_dividendYield() == formatNumber(1 / 3)
        stocks.get(2).getN_dividendYield() == formatNumber(2 / 3)
        stocks.get(3).getN_dividendYield() == 1

        stocks.get(0).getN_trend_ROE() == 0
        stocks.get(1).getN_trend_ROE() == formatNumber(1 / 3)
        stocks.get(2).getN_trend_ROE() == formatNumber(2 / 3)
        stocks.get(3).getN_trend_ROE() == 1

        stocks.get(0).getN_trend_EPS() == 0
        stocks.get(1).getN_trend_EPS() == formatNumber(1 / 3)
        stocks.get(2).getN_trend_EPS() == formatNumber(2 / 3)
        stocks.get(3).getN_trend_EPS() == 1

        stocks.get(0).getN_trend_DebtToEquity() == 1
        stocks.get(1).getN_trend_DebtToEquity() == formatNumber(2 / 3)
        stocks.get(2).getN_trend_DebtToEquity() == formatNumber(1 / 3)
        stocks.get(3).getN_trend_DebtToEquity() == 0
    }

    private double formatNumber(def num) {
        Double.valueOf(df.format(num))
    }
}
