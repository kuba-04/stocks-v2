package com.portfolio.stocks.normalizer

import com.portfolio.stocks.domain.Stock
import com.portfolio.stocks.utils.TestConstants
import org.assertj.core.util.Lists
import spock.lang.Specification

class NormalizedDataProviderTest extends Specification {

    NormalizedDataProvider normalizedDataProvider = new NormalizedDataProvider()

    def PRICE_TO_BOOK = "pb"
    def EPS = "eps"
    def PE_RATIO = "pe"
    def ROE = "roe"
    def ROA = "roa"
    def DIVIDEND_YIELD = "dy"
    def TREND_ROE = "tr_roe"
    def TREND_EPS = "tr_eps"
    def TREND_DEBT_TO_EQUITY = "tr_de"

    private TestConstants testConstants = new TestConstants()

    def "should get normalized PRICE_TO_BOOK"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setPriceToBook(1)

        Stock stock2 = testConstants.getStock()
        stock2.setPriceToBook(2)

        Stock stock3 = testConstants.getStock()
        stock3.setPriceToBook(3)

        Stock stock4 = testConstants.getStock()
        stock4.setPriceToBook(4)

        Stock stock5 = testConstants.getStock()
        stock5.setPriceToBook(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, PRICE_TO_BOOK)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, PRICE_TO_BOOK)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, PRICE_TO_BOOK)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, PRICE_TO_BOOK)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, PRICE_TO_BOOK)

        then:
        result1 == 1.0d
        result2 == 0.75d
        result3 == 0.5d
        result4 == 0.25d
        result5 == 0.0d
    }

    def "should get normalized EPS"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setLatestEPS(1)

        Stock stock2 = testConstants.getStock()
        stock2.setLatestEPS(2)

        Stock stock3 = testConstants.getStock()
        stock3.setLatestEPS(3)

        Stock stock4 = testConstants.getStock()
        stock4.setLatestEPS(4)

        Stock stock5 = testConstants.getStock()
        stock5.setLatestEPS(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, EPS)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, EPS)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, EPS)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, EPS)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, EPS)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized PE_RATIO"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setPeRatio(1)

        Stock stock2 = testConstants.getStock()
        stock2.setPeRatio(2)

        Stock stock3 = testConstants.getStock()
        stock3.setPeRatio(3)

        Stock stock4 = testConstants.getStock()
        stock4.setPeRatio(4)

        Stock stock5 = testConstants.getStock()
        stock5.setPeRatio(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, PE_RATIO)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, PE_RATIO)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, PE_RATIO)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, PE_RATIO)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, PE_RATIO)

        then:
        result1 == 1.0d
        result2 == 0.75d
        result3 == 0.5d
        result4 == 0.25d
        result5 == 0.0d
    }

    def "should get normalized ROE"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setReturnOnEquity(1)

        Stock stock2 = testConstants.getStock()
        stock2.setReturnOnEquity(2)

        Stock stock3 = testConstants.getStock()
        stock3.setReturnOnEquity(3)

        Stock stock4 = testConstants.getStock()
        stock4.setReturnOnEquity(4)

        Stock stock5 = testConstants.getStock()
        stock5.setReturnOnEquity(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, ROE)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, ROE)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, ROE)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, ROE)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, ROE)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized ROA"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setReturnOnAssets(1)

        Stock stock2 = testConstants.getStock()
        stock2.setReturnOnAssets(2)

        Stock stock3 = testConstants.getStock()
        stock3.setReturnOnAssets(3)

        Stock stock4 = testConstants.getStock()
        stock4.setReturnOnAssets(4)

        Stock stock5 = testConstants.getStock()
        stock5.setReturnOnAssets(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, ROA)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, ROA)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, ROA)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, ROA)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, ROA)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized DIVIDEND_YIELD"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setDividendYield(1)

        Stock stock2 = testConstants.getStock()
        stock2.setDividendYield(2)

        Stock stock3 = testConstants.getStock()
        stock3.setDividendYield(3)

        Stock stock4 = testConstants.getStock()
        stock4.setDividendYield(4)

        Stock stock5 = testConstants.getStock()
        stock5.setDividendYield(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, DIVIDEND_YIELD)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, DIVIDEND_YIELD)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, DIVIDEND_YIELD)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, DIVIDEND_YIELD)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, DIVIDEND_YIELD)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized TREND_ROE"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setTrend_ROE(1)

        Stock stock2 = testConstants.getStock()
        stock2.setTrend_ROE(2)

        Stock stock3 = testConstants.getStock()
        stock3.setTrend_ROE(3)

        Stock stock4 = testConstants.getStock()
        stock4.setTrend_ROE(4)

        Stock stock5 = testConstants.getStock()
        stock5.setTrend_ROE(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, TREND_ROE)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, TREND_ROE)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, TREND_ROE)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, TREND_ROE)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, TREND_ROE)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized TREND_EPS"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setTrend_EPS(1)

        Stock stock2 = testConstants.getStock()
        stock2.setTrend_EPS(2)

        Stock stock3 = testConstants.getStock()
        stock3.setTrend_EPS(3)

        Stock stock4 = testConstants.getStock()
        stock4.setTrend_EPS(4)

        Stock stock5 = testConstants.getStock()
        stock5.setTrend_EPS(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, TREND_EPS)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, TREND_EPS)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, TREND_EPS)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, TREND_EPS)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, TREND_EPS)

        then:
        result1 == 0.0d
        result2 == 0.25d
        result3 == 0.5d
        result4 == 0.75d
        result5 == 1.0d
    }

    def "should get normalized TREND_DEBT_TO_EQUITY"() {
        given:
        Stock stock1 = testConstants.getStock()
        stock1.setTrend_DebtToEquity(1)

        Stock stock2 = testConstants.getStock()
        stock2.setTrend_DebtToEquity(2)

        Stock stock3 = testConstants.getStock()
        stock3.setTrend_DebtToEquity(3)

        Stock stock4 = testConstants.getStock()
        stock4.setTrend_DebtToEquity(4)

        Stock stock5 = testConstants.getStock()
        stock5.setTrend_DebtToEquity(5)

        def list = Lists.newArrayList(stock1, stock2, stock3, stock4, stock5)

        when:
        double result1 = normalizedDataProvider.getNormalizedValue(list, stock1, TREND_DEBT_TO_EQUITY)
        double result2 = normalizedDataProvider.getNormalizedValue(list, stock2, TREND_DEBT_TO_EQUITY)
        double result3 = normalizedDataProvider.getNormalizedValue(list, stock3, TREND_DEBT_TO_EQUITY)
        double result4 = normalizedDataProvider.getNormalizedValue(list, stock4, TREND_DEBT_TO_EQUITY)
        double result5 = normalizedDataProvider.getNormalizedValue(list, stock5, TREND_DEBT_TO_EQUITY)

        then:
        result1 == 1.0d
        result2 == 0.75d
        result3 == 0.5d
        result4 == 0.25d
        result5 == 0.0d
    }
}
