package com.portfolio.stocks.utils

import com.portfolio.stocks.domain.Stock
import lombok.Getter
import lombok.NoArgsConstructor
import org.assertj.core.util.Lists

@Getter
@NoArgsConstructor
class TestConstants {

    String id = "testId"
    String ticker = "TST"
    String name = "testName"
    String sector = "testSector"
    String industry = "testIndustry"
    String description = "Exxon Mobil Corp is an integrated oil and gas company. " +
            "It is engaged in exploration for, and production of, crude oil and natural gas. " +
            "It is also engaged in manufacturing, transportation and sale of crude oil, " +
            "natural gas and petroleum products."
    double price = 1.0
    double priceToBook = 1.1
    double latestEPS = 1.2
    double peRatio = 1.3
    double returnOnEquity = 1.4
    double returnOnAssets = 1.5
    double dividendYield = 1.6
    double trend_ROE = 1.7
    double trend_EPS = 1.8
    double trend_DebtToEquity = 1.9
    double roe_0 = 2.0
    double roe_1 = 2.1
    double roe_2 = 2.2
    double roe_3 = 2.3
    double actualEPS_0 = 2.4
    double actualEPS_1 = 2.5
    double actualEPS_2 = 2.6
    double actualEPS_3 = 2.7
    double debtToEquity_0 = 2.8
    double debtToEquity_1 = 2.9
    double debtToEquity_2 = 3.0
    double debtToEquity_3 = 3.1
    double npriceToBook = 3.2
    double nlatestEPS = 3.3
    double npeRatio = 3.4
    double nreturnOnEquity = 3.5
    double nreturnOnAssets = 3.6
    double ndividendYield = 3.7
    double n_trend_ROE = 3.8
    double n_trend_EPS = 3.9
    double n_trend_DebtToEquity = 4.0
    double sortOrder = 4.1
    List<String> sortOrderList = Lists.emptyList()
    String portfolio = "testPortfolio"
    String owner = "testOwner"

    def getStock() {
        new Stock(
                id,
                ticker,
                name,
                sector,
                industry,
                description,
                price,
                priceToBook,
                latestEPS,
                peRatio,
                returnOnEquity,
                returnOnAssets,
                dividendYield,
                trend_ROE,
                trend_EPS,
                trend_DebtToEquity,
                roe_0,
                roe_1,
                roe_2,
                roe_3,
                actualEPS_0,
                actualEPS_1,
                actualEPS_2,
                actualEPS_3,
                debtToEquity_0,
                debtToEquity_1,
                debtToEquity_2,
                debtToEquity_3,
                npriceToBook,
                nlatestEPS,
                npeRatio,
                nreturnOnEquity,
                nreturnOnAssets,
                ndividendYield,
                n_trend_ROE,
                n_trend_EPS,
                n_trend_DebtToEquity,
                sortOrder,
                sortOrderList,
                portfolio,
                owner
        )
    }

    def getStock(String ticker) {
        new Stock(
                id,
                ticker,
                name,
                sector,
                industry,
                description,
                price,
                priceToBook,
                latestEPS,
                peRatio,
                returnOnEquity,
                returnOnAssets,
                dividendYield,
                trend_ROE,
                trend_EPS,
                trend_DebtToEquity,
                roe_0,
                roe_1,
                roe_2,
                roe_3,
                actualEPS_0,
                actualEPS_1,
                actualEPS_2,
                actualEPS_3,
                debtToEquity_0,
                debtToEquity_1,
                debtToEquity_2,
                debtToEquity_3,
                npriceToBook,
                nlatestEPS,
                npeRatio,
                nreturnOnEquity,
                nreturnOnAssets,
                ndividendYield,
                n_trend_ROE,
                n_trend_EPS,
                n_trend_DebtToEquity,
                sortOrder,
                sortOrderList,
                portfolio,
                owner
        )
    }

    def getPreNormalizedStock(String ticker) {
        new Stock(
                id,
                ticker,
                name,
                sector,
                industry,
                description,
                price,
                priceToBook,
                latestEPS,
                peRatio,
                returnOnEquity,
                returnOnAssets,
                dividendYield,
                trend_ROE,
                trend_EPS,
                trend_DebtToEquity,
                roe_0,
                roe_1,
                roe_2,
                roe_3,
                actualEPS_0,
                actualEPS_1,
                actualEPS_2,
                actualEPS_3,
                debtToEquity_0,
                debtToEquity_1,
                debtToEquity_2,
                debtToEquity_3,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                sortOrderList,
                portfolio,
                owner
        )
    }

    def getStock(int suffix) {
        new Stock(
                id + suffix,
                ticker + suffix,
                name + suffix,
                sector + suffix,
                industry + suffix,
                description + suffix,
                price + suffix,
                priceToBook + suffix,
                latestEPS + suffix,
                peRatio + suffix,
                returnOnEquity + suffix,
                returnOnAssets + suffix,
                dividendYield + suffix,
                trend_ROE + suffix,
                trend_EPS + suffix,
                trend_DebtToEquity + suffix,
                roe_0 + suffix,
                roe_1 + suffix,
                roe_2 + suffix,
                roe_3 + suffix,
                actualEPS_0 + suffix,
                actualEPS_1 + suffix,
                actualEPS_2 + suffix,
                actualEPS_3 + suffix,
                debtToEquity_0 + suffix,
                debtToEquity_1 + suffix,
                debtToEquity_2 + suffix,
                debtToEquity_3 + suffix,
                npriceToBook + suffix,
                nlatestEPS + suffix,
                npeRatio + suffix,
                nreturnOnEquity + suffix,
                nreturnOnAssets + suffix,
                ndividendYield + suffix,
                n_trend_ROE + suffix,
                n_trend_EPS + suffix,
                n_trend_DebtToEquity + suffix,
                sortOrder + suffix,
                sortOrderList,
                portfolio + suffix,
                owner + suffix
        )
    }

    def stock(Stock stock) {
        return new Stock(
                stock.getId(),
                stock.getTicker(),
                stock.getName(),
                stock.getSector(),
                stock.getIndustry(),
                stock.getDescription(),
                stock.getPrice(),
                stock.getPriceToBook(),
                stock.getLatestEPS(),
                stock.getPeRatio(),
                stock.getReturnOnEquity(),
                stock.getReturnOnAssets(),
                stock.getDividendYield(),
                stock.getTrend_ROE(),
                stock.getTrend_EPS(),
                stock.getTrend_DebtToEquity(),
                stock.getRoe_0(),
                stock.getRoe_1(),
                stock.getRoe_2(),
                stock.getRoe_3(),
                stock.getActualEPS_0(),
                stock.getActualEPS_1(),
                stock.getActualEPS_2(),
                stock.getActualEPS_3(),
                stock.getDebtToEquity_0(),
                stock.getDebtToEquity_1(),
                stock.getDebtToEquity_2(),
                stock.getDebtToEquity_3(),
                stock.getN_priceToBook(),
                stock.getN_latestEPS(),
                stock.getN_peRatio(),
                stock.getN_returnOnEquity(),
                stock.getN_returnOnAssets(),
                stock.getN_dividendYield(),
                stock.getN_trend_ROE(),
                stock.getN_trend_EPS(),
                stock.getN_trend_DebtToEquity(),
                stock.getSortOrder(),
                stock.getSortOrderList(),
                stock.getPortfolio(),
                stock.getOwner())
    }
}
