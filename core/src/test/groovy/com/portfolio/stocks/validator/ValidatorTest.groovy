package com.portfolio.stocks.validator

import com.portfolio.stocks.domain.Stock
import com.portfolio.stocks.exceptions.EquityAlreadyExistsException
import com.portfolio.stocks.repository.InMemoryStockRepository
import com.portfolio.stocks.utils.TestConstants
import com.portfolio.stocks.validators.IEXValidator
import spock.lang.Specification

class ValidatorTest extends Specification {

    def stockRepository = new InMemoryStockRepository()
    
    def validator = new IEXValidator(stockRepository)
    def testConstants = new TestConstants()

    def "should throw EquityAlreadyExistsException"() {
        given:
        def stock = new Stock()
        stock.setOwner(testConstants.owner)
        stock.setTicker(testConstants.ticker)
        stockRepository.save(stock)

        when:
        validator.validateIfAlreadyExists(testConstants.owner, testConstants.ticker)

        then:
        thrown EquityAlreadyExistsException
    }

    def "should not throw Exception"() {
        setup:
        def stock = new Stock()
        stock.setOwner(testConstants.owner)
        stock.setTicker(testConstants.ticker)
        stockRepository.save(stock)

        expect:
        validator.validateIfAlreadyExists(testConstants.owner, testConstants.ticker + ".1")
    }
}
