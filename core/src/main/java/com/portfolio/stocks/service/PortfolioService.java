package com.portfolio.stocks.service;

import com.portfolio.stocks.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;

@Slf4j
@Service
@RequiredArgsConstructor
public class PortfolioService {

    private final StockRepository stockRepository;

    @Transactional
    public LinkedHashSet<String> getPortfolios(final String owner) {
        LinkedHashSet<String> portfolios = new LinkedHashSet<>();
        stockRepository.findAllByOwner(owner)
                .forEach(stock -> portfolios.add(stock.getPortfolio()));
        return portfolios;
    }

    @Transactional
    public void deletePortfolio(final String owner, final String portfolio) {
        stockRepository.deleteAllByOwnerAndPortfolio(owner, portfolio);
    }
}
