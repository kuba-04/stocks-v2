package com.portfolio.stocks.service;

import com.portfolio.stocks.client.IEXStockCreator;
import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.dtos.StockDto;
import com.portfolio.stocks.repository.StockRepository;
import com.portfolio.stocks.service.sorting.SortingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class StockService {

    private final StockRepository stockRepository;
    private final IEXStockCreator iexStockCreator;
    private final SortingService sortingService;

    @Transactional
    public List<Stock> addStock(final StockDto stockDto) throws IOException {
        Stock stock = iexStockCreator.createIEXStock(stockDto.getOwner(), stockDto.getPortfolio(), stockDto.getTicker());
        stockRepository.save(stock);
        log.info("{} added to database", stock.getTicker());
        return sortingService.getStocksWithExistingOrder(stockDto.getOwner(), stockDto.getPortfolio());
    }

    @Transactional
    public List<Stock> deleteStock(final String owner, final String ticker) {
        String portfolio = stockRepository.findByOwnerAndTicker(owner, ticker).get().getPortfolio();
        stockRepository.deleteByOwnerAndTicker(owner, ticker.toUpperCase());
        log.info("{} deleted from database", ticker.toUpperCase());
        return sortingService.getStocksWithExistingOrder(owner, portfolio);
    }
}
