package com.portfolio.stocks.service.sorting;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.normalizer.DataNormalizer;
import com.portfolio.stocks.normalizer.ParamConstants;
import com.portfolio.stocks.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import static com.portfolio.stocks.normalizer.ParamConstants.DIVIDEND_YIELD;
import static com.portfolio.stocks.normalizer.ParamConstants.EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.PE_RATIO;
import static com.portfolio.stocks.normalizer.ParamConstants.PRICE_TO_BOOK;
import static com.portfolio.stocks.normalizer.ParamConstants.ROA;
import static com.portfolio.stocks.normalizer.ParamConstants.ROE;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_DEBT_TO_EQUITY;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_ROE;

@Slf4j
@Service
@RequiredArgsConstructor
public class SortingService {

    private final StockRepository stockRepository;
    private final DataNormalizer normalizer;

    @Transactional
    public List<Stock> getStocksWithExistingOrder(final String owner, final String portfolio) {
        List<Stock> stocks = stockRepository.findAllByOwnerAndPortfolio(owner, portfolio);
        if (stocks.size() <= 1) {
            return stocks;
        } else {
            List<Stock> normalized = normalizer.normalize(stocks);
            return normalized.stream()
                    .map(stock -> applyNewSortOrder(stock, getCustomSortingOrder(stock.getSortOrderList(), stock)))
                    .sorted(Comparator.comparing(Stock::getSortOrder).reversed())
                    .collect(Collectors.toList());
        }
    }

    @Transactional
    public List<Stock> getStocksWithDefaultOrder(final String owner, final String portfolio) {
        List<Stock> stocks = stockRepository.findAllByOwnerAndPortfolio(owner, portfolio);
        if (stocks.size() <= 1) {
            return stocks;
        } else {
            List<Stock> normalized = normalizer.normalize(stocks);
            return normalized.stream()
                    .map(stock -> applyNewSortOrder(stock, getDefaultSortOrder(stock)))
                    .sorted(Comparator.comparing(Stock::getSortOrder).reversed())
                    .collect(Collectors.toList());
        }
    }

    @Transactional
    public List<Stock> getStocksWithCustomOrder(final String owner, final List<String> sortingOrders, final String portfolio) {
        List<Stock> stocks = stockRepository.findAllByOwnerAndPortfolio(owner, portfolio);
        if (stocks.size() <= 1) {
            return stocks;
        } else {
            List<Stock> normalized = normalizer.normalize(stocks);
            return normalized.stream()
                    .map(stock -> applyNewSortOrder(stock, getCustomSortingOrder(sortingOrders, stock)))
                    .sorted(Comparator.comparing(Stock::getSortOrder).reversed())
                    .collect(Collectors.toList());
        }
    }

    private Map<Double, List<String>> getDefaultSortOrder(final Stock stock) {
        Map<Double, List<String>> map = new HashMap<>();
        double sum = DoubleStream.builder()
                .add(stock.getN_priceToBook())
                .add(stock.getN_latestEPS())
                .add(stock.getN_peRatio())
                .add(stock.getN_returnOnEquity())
                .add(stock.getN_returnOnAssets())
                .add(stock.getN_dividendYield())
                .add(stock.getN_trend_ROE())
                .add(stock.getN_trend_EPS())
                .add(stock.getN_trend_DebtToEquity())
                .build().sum();
        map.put(sum, Collections.singletonList("default"));
        return map;
    }

    private Map<Double, List<String>> getCustomSortingOrder(final List<String> sortingOrders, final Stock stock) {
        double sum = 0.0;
        List<String> list = new ArrayList<>();
        Map<Double, List<String>> map = new HashMap<>();
        if (sortingOrders.contains(PRICE_TO_BOOK)) {
            sum += stock.getN_priceToBook();
            list.add(PRICE_TO_BOOK);
        }
        if (sortingOrders.contains(EPS)) {
            sum += stock.getN_latestEPS();
            list.add(EPS);
        }
        if (sortingOrders.contains(PE_RATIO)) {
            sum += stock.getN_peRatio();
            list.add(PE_RATIO);
        }
        if (sortingOrders.contains(ROE)) {
            sum += stock.getN_returnOnEquity();
            list.add(ROE);
        }
        if (sortingOrders.contains(ROA)) {
            sum += stock.getN_returnOnAssets();
            list.add(ROA);
        }
        if (sortingOrders.contains(DIVIDEND_YIELD)) {
            sum += stock.getN_dividendYield();
            list.add(DIVIDEND_YIELD);
        }
        if (sortingOrders.contains(TREND_ROE)) {
            sum += stock.getN_trend_ROE();
            list.add(TREND_ROE);
        }
        if (sortingOrders.contains(TREND_EPS)) {
            sum += stock.getN_trend_EPS();
            list.add(TREND_EPS);
        }
        if (sortingOrders.contains(TREND_DEBT_TO_EQUITY)) {
            sum += stock.getN_trend_DebtToEquity();
            list.add(TREND_DEBT_TO_EQUITY);
        }
        if (list.containsAll(ParamConstants.allParams())) {
            list = Collections.singletonList("default");
        }
        map.put(sum, list);
        return map;
    }

    @Transactional
    private Stock applyNewSortOrder(Stock stock, Map<Double, List<String>> sortOrder) {
        stock.setSortOrder(sortOrder.keySet().iterator().next());
        stock.setSortOrderList(sortOrder.values().iterator().next());
        log.info("updating sort order for " + stock.getTicker());
        return stockRepository.save(stock);
    }
}
