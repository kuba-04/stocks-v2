package com.portfolio.stocks.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.LinkedList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "stocks")
public class Stock {

    @Id
    private String id;
    private String ticker;
    private String name;
    private String sector;
    private String industry;
    private String description;
    private double price;
    private double priceToBook;
    private double latestEPS;
    private double peRatio;
    private double returnOnEquity;
    private double returnOnAssets;
    private double dividendYield;
    private double trend_ROE;
    private double trend_EPS;
    private double trend_DebtToEquity;
    private double roe_0;
    private double roe_1;
    private double roe_2;
    private double roe_3;
    private double actualEPS_0;
    private double actualEPS_1;
    private double actualEPS_2;
    private double actualEPS_3;
    private double debtToEquity_0;
    private double debtToEquity_1;
    private double debtToEquity_2;
    private double debtToEquity_3;
    private double n_priceToBook;
    private double n_latestEPS;
    private double n_peRatio;
    private double n_returnOnEquity;
    private double n_returnOnAssets;
    private double n_dividendYield;
    private double n_trend_ROE;
    private double n_trend_EPS;
    private double n_trend_DebtToEquity;
    private double sortOrder;
    private List<String> sortOrderList = new LinkedList<>();
    private String portfolio;
    private String owner;
}
