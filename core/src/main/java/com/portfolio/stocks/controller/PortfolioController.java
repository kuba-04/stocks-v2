package com.portfolio.stocks.controller;

import com.portfolio.stocks.service.PortfolioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashSet;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/v2/portfolios")
@CrossOrigin(origins = "*")
public class PortfolioController {

    private final PortfolioService portfolioService;

    @GetMapping(value = "/{owner}")
    public LinkedHashSet<String> getPortfolios(@PathVariable final String owner) {
        return portfolioService.getPortfolios(owner);
    }

    @DeleteMapping(value = "/{owner}/{portfolio}")
    public void deletePortfolio(@PathVariable final String owner, @PathVariable final String portfolio) {
        portfolioService.deletePortfolio(owner, portfolio);
    }
}
