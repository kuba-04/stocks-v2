package com.portfolio.stocks.controller;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.dtos.StockDto;
import com.portfolio.stocks.service.StockService;
import com.portfolio.stocks.service.sorting.SortingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v2/stocks/")
@CrossOrigin(origins = "*")
public class StockController {

    private final SortingService sortingService;
    private final StockService stockService;

    @GetMapping(value = "/{owner}/{portfolio}")
    public List<Stock> getPortfolioStocks(@PathVariable final String owner, @PathVariable final String portfolio) {
        return sortingService.getStocksWithExistingOrder(owner, portfolio);
    }

    @PostMapping("/add")
    public List<Stock> addStock(@RequestBody StockDto stockDto) throws IOException {
        return stockService.addStock(stockDto);
    }

    @PostMapping("delete")
    public List<Stock> deleteStock(@RequestBody StockDto stockDto) {
        return stockService.deleteStock(stockDto.getOwner(), stockDto.getTicker());
    }
}
