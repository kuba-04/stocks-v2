package com.portfolio.stocks.controller;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.dtos.SortingOrderDto;
import com.portfolio.stocks.service.sorting.SortingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v2/sorting-service/")
@CrossOrigin(origins = "*")
public class SortingController {

    private final SortingService sortingService;

    @PostMapping("custom")
    public List<Stock> retrieveWithCustomSortingOrder(@RequestBody SortingOrderDto sortingOrderDto) throws UnsupportedEncodingException {
        return sortingService.getStocksWithCustomOrder(
                sortingOrderDto.getOwner(),
                Arrays.asList(sortingOrderDto.getSorting()),
                URLDecoder.decode(sortingOrderDto.getPortfolio(), "UTF-8"));
    }

    @PostMapping("default")
    public List<Stock> retrieveWithDefaultSortingOrder(@RequestBody SortingOrderDto sortingOrderDto) throws UnsupportedEncodingException {
        return sortingService.getStocksWithDefaultOrder(
                sortingOrderDto.getOwner(),
                URLDecoder.decode(sortingOrderDto.getPortfolio(), "UTF-8"));
    }
}
