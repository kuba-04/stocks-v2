package com.portfolio.stocks.dtos;

import lombok.Getter;

@Getter
public class StockDto {

    private String owner;
    private String portfolio;
    private String ticker;
}
