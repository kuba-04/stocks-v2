package com.portfolio.stocks.dtos;

import lombok.Getter;

@Getter
public class SortingOrderDto {

    private String owner;
    private String portfolio;
    private String[] sorting;
}
