package com.portfolio.stocks.normalizer;

import java.util.Arrays;
import java.util.List;

public class ParamConstants {

    public static final String PRICE_TO_BOOK = "pb";
    public static final String EPS = "eps";
    public static final String PE_RATIO = "pe";
    public static final String ROE = "roe";
    public static final String ROA = "roa";
    public static final String DIVIDEND_YIELD = "dy";
    public static final String TREND_ROE = "tr_roe";
    public static final String TREND_EPS = "tr_eps";
    public static final String TREND_DEBT_TO_EQUITY = "tr_de";

    public static List<String> allParams() {
        return Arrays.asList(
                PRICE_TO_BOOK,
                EPS,
                PE_RATIO,
                ROE,
                ROA,
                DIVIDEND_YIELD,
                TREND_ROE,
                TREND_EPS,
                TREND_DEBT_TO_EQUITY);
    }
}
