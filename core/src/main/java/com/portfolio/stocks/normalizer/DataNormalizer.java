package com.portfolio.stocks.normalizer;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.exceptions.UnableToNormalizeResourceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.portfolio.stocks.normalizer.ParamConstants.DIVIDEND_YIELD;
import static com.portfolio.stocks.normalizer.ParamConstants.EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.PE_RATIO;
import static com.portfolio.stocks.normalizer.ParamConstants.PRICE_TO_BOOK;
import static com.portfolio.stocks.normalizer.ParamConstants.ROA;
import static com.portfolio.stocks.normalizer.ParamConstants.ROE;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_DEBT_TO_EQUITY;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_ROE;

@Slf4j
@RequiredArgsConstructor
@Service
public class DataNormalizer {

    private final NormalizedDataProvider normalizedDataProvider;

    public List<Stock> normalize(List<Stock> stocks) throws UnableToNormalizeResourceException {
        return stocks.stream()
                .map(stock -> applyNewNormalizedValues(stocks, stock))
                .collect(Collectors.toList());
    }

    @Transactional
    private Stock applyNewNormalizedValues(final List<Stock> stocks, final Stock stock) throws UnableToNormalizeResourceException {
        stock.setN_priceToBook(normalizedDataProvider.getNormalizedValue(stocks, stock, PRICE_TO_BOOK));
        stock.setN_latestEPS(normalizedDataProvider.getNormalizedValue(stocks, stock, EPS));
        stock.setN_peRatio(normalizedDataProvider.getNormalizedValue(stocks, stock, PE_RATIO));
        stock.setN_returnOnEquity(normalizedDataProvider.getNormalizedValue(stocks, stock, ROE));
        stock.setN_returnOnAssets(normalizedDataProvider.getNormalizedValue(stocks, stock, ROA));
        stock.setN_dividendYield(normalizedDataProvider.getNormalizedValue(stocks, stock, DIVIDEND_YIELD));
        stock.setN_trend_ROE(normalizedDataProvider.getNormalizedValue(stocks, stock, TREND_ROE));
        stock.setN_trend_EPS(normalizedDataProvider.getNormalizedValue(stocks, stock, TREND_EPS));
        stock.setN_trend_DebtToEquity(normalizedDataProvider.getNormalizedValue(stocks, stock, TREND_DEBT_TO_EQUITY));
        log.info("Parameters for " + stock.getTicker() + " normalized successfully");
        return stock;
    }
}
