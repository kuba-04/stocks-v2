package com.portfolio.stocks.normalizer;

import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.exceptions.UnableToNormalizeResourceException;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;

import static com.portfolio.stocks.normalizer.ParamConstants.DIVIDEND_YIELD;
import static com.portfolio.stocks.normalizer.ParamConstants.EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.PE_RATIO;
import static com.portfolio.stocks.normalizer.ParamConstants.PRICE_TO_BOOK;
import static com.portfolio.stocks.normalizer.ParamConstants.ROA;
import static com.portfolio.stocks.normalizer.ParamConstants.ROE;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_DEBT_TO_EQUITY;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_EPS;
import static com.portfolio.stocks.normalizer.ParamConstants.TREND_ROE;

@Service
public class NormalizedDataProvider {

    private static DecimalFormat df = new DecimalFormat(".####");

    public double getNormalizedValue(final List<Stock> stocks, final Stock stock, final String param) throws UnableToNormalizeResourceException {
        double actualValue;
        double normalizedValue;
        switch(param) {
            case PRICE_TO_BOOK:
                actualValue = stock.getPriceToBook();
                normalizedValue = 1 - normalize(stocks, actualValue, param);
                break;
            case EPS:
                actualValue = stock.getLatestEPS();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case PE_RATIO:
                actualValue = stock.getPeRatio();
                if (actualValue < 0.0) {
                    actualValue = 0.0;
                }
                normalizedValue = 1 - normalize(stocks, actualValue, param);
                break;
            case ROE:
                actualValue = stock.getReturnOnEquity();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case ROA:
                actualValue = stock.getReturnOnAssets();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case DIVIDEND_YIELD:
                actualValue = stock.getDividendYield();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case TREND_ROE:
                actualValue = stock.getTrend_ROE();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case TREND_EPS:
                actualValue = stock.getTrend_EPS();
                normalizedValue = normalize(stocks, actualValue, param);
                break;
            case TREND_DEBT_TO_EQUITY:
                actualValue = stock.getTrend_DebtToEquity();
                normalizedValue = 1 - normalize(stocks, actualValue, param);
                break;
            default:
                throw new UnableToNormalizeResourceException();
        }
        if (Double.valueOf(normalizedValue).isNaN()) {
            return actualValue;
        }
        return Double.valueOf(df.format(normalizedValue));
    }

    private double normalize(final List<Stock> stocks, final double actualValue, final String param) throws UnableToNormalizeResourceException {
        return (actualValue - getMin(stocks, param)) / (getMax(stocks, param) - getMin(stocks, param));
    }

    private double getMin(final List<Stock> stocks, final String param) throws UnableToNormalizeResourceException {
        return getMinParamFromList(stocks, param);
    }

    private double getMinParamFromList(final List<Stock> stocks, final String param) {
        double value;
        switch (param) {
            case PRICE_TO_BOOK:
                value = stocks.stream().parallel()
                        .map(Stock::getPriceToBook)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case EPS:
                value = stocks.stream().parallel()
                        .map(Stock::getLatestEPS)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case PE_RATIO:
                value = stocks.stream().parallel()
                        .map(Stock::getPeRatio)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case ROE:
                value = stocks.stream().parallel()
                        .map(Stock::getReturnOnEquity)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case ROA:
                value = stocks.stream().parallel()
                        .map(Stock::getReturnOnAssets)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case DIVIDEND_YIELD:
                value = stocks.stream().parallel()
                        .map(Stock::getDividendYield)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_ROE:
                value = stocks.stream().parallel()
                        .map(Stock::getTrend_ROE)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_EPS:
                value = stocks.stream().parallel()
                        .map(Stock::getTrend_EPS)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_DEBT_TO_EQUITY:
                value = stocks.stream().parallel()
                        .map(Stock::getTrend_DebtToEquity)
                        .min(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            default:
                throw new UnableToNormalizeResourceException();
        }
        return value;
    }

    private double getMaxParamFromList(final String param, final List<Stock> stockList) {
        double value;
        switch(param) {
            case PRICE_TO_BOOK:
                value = stockList.stream().parallel()
                        .map(Stock::getPriceToBook)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case EPS:
                value = stockList.stream().parallel()
                        .map(Stock::getLatestEPS)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case PE_RATIO:
                value = stockList.stream().parallel()
                        .map(Stock::getPeRatio)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case ROE:
                value = stockList.stream().parallel()
                        .map(Stock::getReturnOnEquity)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case ROA:
                value = stockList.stream().parallel()
                        .map(Stock::getReturnOnAssets)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case DIVIDEND_YIELD:
                value = stockList.stream().parallel()
                        .map(Stock::getDividendYield)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_ROE:
                value = stockList.stream().parallel()
                        .map(Stock::getTrend_ROE)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_EPS:
                value = stockList.stream().parallel()
                        .map(Stock::getTrend_EPS)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            case TREND_DEBT_TO_EQUITY:
                value = stockList.stream().parallel()
                        .map(Stock::getTrend_DebtToEquity)
                        .max(Comparator.naturalOrder()).orElseThrow(UnableToNormalizeResourceException::new);
                break;
            default:
                throw new UnableToNormalizeResourceException();

        }
        return value;
    }

    private double getMax(final List<Stock> stocks, final String param) throws UnableToNormalizeResourceException {
        return getMaxParamFromList(param, stocks);
    }
}
