package com.portfolio.stocks.scheduler;

import com.portfolio.stocks.client.data.retriever.trend.calculator.DataRetrieverService;
import com.portfolio.stocks.client.data.retriever.trend.calculator.trend.calculator.TrendCalculator;
import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class PriceRefreshScheduler {

    private static final long FIFTEEN_MINUTES = 15 * 60_000;
    private static final long TWENTYFOUR_HOURS = 24 * 60 * 60_000;

    private final StockRepository stockRepository;
    private final DataRetrieverService dataRetrieverService;
    private final TrendCalculator trendCalculator;

    @Transactional
    @Scheduled(fixedDelay = FIFTEEN_MINUTES)
    public void refreshPrice() throws Exception {
        for (Stock stock : stockRepository.findAll()) {
            stock.setPrice(dataRetrieverService.retrievePrice(stock.getTicker()));
            log.info("equity: " + stock.getTicker() + ", price refresh: " + stock.getPrice());
            stockRepository.save(stock);
        }
    }

    @Transactional
    @Scheduled(fixedDelay = TWENTYFOUR_HOURS)
    public void refreshStats() throws Exception {
        for (Stock stock : stockRepository.findAll()) {

            double roe_0 = retrieve(stock, "roe_0");
            double roe_1 = retrieve(stock, "roe_1");
            double roe_2 = retrieve(stock, "roe_2");
            double roe_3 = retrieve(stock, "roe_3");

            double actualEPS_0 = retrieve(stock, "actualEPS_0");
            double actualEPS_1 = retrieve(stock, "actualEPS_1");
            double actualEPS_2 = retrieve(stock, "actualEPS_2");
            double actualEPS_3 = retrieve(stock, "actualEPS_3");

            double debtToEquity_0 = retrieve(stock, "debtToEquity_0");
            double debtToEquity_1 = retrieve(stock, "debtToEquity_1");
            double debtToEquity_2 = retrieve(stock, "debtToEquity_2");
            double debtToEquity_3 = retrieve(stock, "debtToEquity_3");

            double trend_ROE = trendCalculator.calculateTrend(
                    roe_0, roe_1, roe_2, roe_3);
            double trend_EPS = trendCalculator.calculateTrend(
                    actualEPS_0, actualEPS_1, actualEPS_2, actualEPS_3);
            double trend_DebtToEquity = -trendCalculator.calculateTrend(
                    debtToEquity_0, debtToEquity_1, debtToEquity_2, debtToEquity_3);

            stock.setPrice(dataRetrieverService.retrievePrice(stock.getTicker()));
            stock.setPriceToBook(retrieve(stock, "priceToBook"));
            stock.setLatestEPS(retrieve(stock, "latestEPS"));
            stock.setPeRatio(retrieve(stock, "peRatio"));
            stock.setReturnOnEquity(retrieve(stock, "returnOnEquity"));
            stock.setReturnOnAssets(retrieve(stock, "returnOnAssets"));
            stock.setDividendYield(retrieve(stock, "dividendYield"));
            stock.setRoe_0(roe_0);
            stock.setRoe_1(roe_1);
            stock.setRoe_2(roe_2);
            stock.setRoe_3(roe_3);
            stock.setActualEPS_0(actualEPS_0);
            stock.setActualEPS_1(actualEPS_1);
            stock.setActualEPS_2(actualEPS_2);
            stock.setActualEPS_3(actualEPS_3);
            stock.setDebtToEquity_0(debtToEquity_0);
            stock.setDebtToEquity_1(debtToEquity_1);
            stock.setDebtToEquity_2(debtToEquity_2);
            stock.setDebtToEquity_3(debtToEquity_3);
            stock.setTrend_ROE(trend_ROE);
            stock.setTrend_EPS(trend_EPS);
            stock.setTrend_DebtToEquity(trend_DebtToEquity);

            log.info("equity: " + stock.getTicker() + " - daily update");
            stockRepository.save(stock);
        }
    }

    private double retrieve(final Stock stock, final String param) throws IOException {
        return dataRetrieverService.retrieveStats(stock.getTicker()).get(param);
    }
}
