package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.CONFLICT)
public class EquityAlreadyExistsException extends RuntimeException {
    public EquityAlreadyExistsException(final String ticker) {
        super("already exists!");
        log.error(ticker.toUpperCase() + " already exists in database");
    }
}
