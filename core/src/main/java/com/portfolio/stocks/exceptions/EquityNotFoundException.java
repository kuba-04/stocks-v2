package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;

@Slf4j
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EquityNotFoundException extends IOException {
    public EquityNotFoundException() {
        super("doesn't exist!");
        log.error("Unable to retrieve data from IEXTrading API");
    }
}
