package com.portfolio.stocks.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class ValueNotFoundException extends RuntimeException {
    public ValueNotFoundException() {
        log.error("property is empty");
    }
}
