package com.portfolio.stocks.exceptions;

public class UnableToNormalizeResourceException extends RuntimeException {
    public UnableToNormalizeResourceException() {
        super("Failed to normalize data");
    }
}
