package com.portfolio.stocks.client;

import com.portfolio.stocks.config.ClientConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@RequiredArgsConstructor
@Component
public class IEXTradingClient {

    private final ClientConfiguration clientConfiguration;

    public URL getPriceUrl(final String ticker) throws MalformedURLException {
        return new URL(clientConfiguration.getApiEndpoint() + ticker + "/price");
    }

    public URL getFinancialsDescriptionUrl(final String ticker) throws MalformedURLException {
        return new URL(clientConfiguration.getApiEndpoint() + ticker + "/batch?types=company,price,stats,financials,earnings");
    }

    public URL getFinancialsUrl(final String ticker) throws MalformedURLException {
        return new URL(clientConfiguration.getApiEndpoint() + ticker + "/batch?types=price,stats,financials,earnings");
    }

}
