package com.portfolio.stocks.client.data.retriever.trend.calculator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.portfolio.stocks.client.IEXTradingClient;
import com.portfolio.stocks.exceptions.EquityNotFoundException;
import com.portfolio.stocks.exceptions.NodeNotFoundException;
import com.portfolio.stocks.exceptions.ValueNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Double.valueOf;

@Slf4j
@RequiredArgsConstructor
@Service
public class DataRetrieverService {

    private static final int MAX_DESCRIPTION_LENGTH = 254;

    private final IEXTradingClient iexTradingClient;
    private final ObjectMapper mapper;

    private DecimalFormat df = new DecimalFormat(".####");

    public double retrievePrice(final String ticker) throws EquityNotFoundException {
        JsonNode jsonNode;
        try {
            jsonNode = mapper.readTree(iexTradingClient.getPriceUrl(ticker));
        } catch (IOException e) {
            throw new EquityNotFoundException();
        }
        return jsonNode.asDouble();
    }

    public Map<String, Double> retrieveStats(final String ticker) throws EquityNotFoundException {
        JsonNode root;
        try {
            root = mapper.readTree(iexTradingClient.getFinancialsUrl(ticker));
        } catch (IOException e) {
            throw new EquityNotFoundException();
        }

        return mapStats(root);
    }

    public Map<String, Object> retrieveStatsAndDescription(final String ticker) throws EquityNotFoundException {
        JsonNode root;
        try {
            root = mapper.readTree(iexTradingClient.getFinancialsDescriptionUrl(ticker));
        } catch (IOException e) {
            throw new EquityNotFoundException();
        }

        Map<String, Object> map = new HashMap<>();
        map.putAll(mapDescriptions(root));
        map.putAll(mapStats(root));

        return map;
    }

    private Map<String, String> mapDescriptions(final JsonNode node) {
        Map<String, String> map = new HashMap<>();
        map.put("companyName", node.path("company").get("companyName").asText());
        map.put("industry", node.path("company").get("industry").asText());
        map.put("sector", node.path("company").get("sector").asText());

        String description = node.path("company").get("description").asText();
        if (description.length() > MAX_DESCRIPTION_LENGTH) {
            description = description.substring(0, MAX_DESCRIPTION_LENGTH - 4).concat("...");
        }
        map.put("description", description);
        return map;
    }

    private Map<String, Double> mapStats(final JsonNode node) {
        Map<String, Double> map = new HashMap<>();

        map.put("price", valueOf(df.format(getPrice(node))));
        map.put("latestEPS", valueOf(df.format(getLatestEPS(node))));
        map.put("peRatio", valueOf(df.format(calculatePERatio(getPrice(node), getLatestEPS(node)))));
        map.put("priceToBook", valueOf(df.format(node.path("stats").get("priceToBook").asDouble())));
        map.put("returnOnEquity", valueOf(df.format(node.path("stats").get("returnOnEquity").asDouble())));
        map.put("returnOnAssets", valueOf(df.format(node.path("stats").get("returnOnAssets").asDouble())));
        map.put("dividendYield", valueOf(df.format(node.path("stats").get("dividendYield").asDouble())));

        map.put("roe_0", valueOf(df.format(getROEByQuarter(node, 0))));
        map.put("roe_1", valueOf(df.format(getROEByQuarter(node, 1))));
        map.put("roe_2", valueOf(df.format(getROEByQuarter(node, 2))));
        map.put("roe_3", valueOf(df.format(getROEByQuarter(node, 3))));

        map.put("actualEPS_0", valueOf(df.format(getEPSByQuarter(node, 0))));
        map.put("actualEPS_1", valueOf(df.format(getEPSByQuarter(node, 1))));
        map.put("actualEPS_2", valueOf(df.format(getEPSByQuarter(node, 2))));
        map.put("actualEPS_3", valueOf(df.format(getEPSByQuarter(node, 3))));

        map.put("debtToEquity_0", valueOf(df.format(getDebtToEquityByQuarter(node, 0))));
        map.put("debtToEquity_1", valueOf(df.format(getDebtToEquityByQuarter(node, 1))));
        map.put("debtToEquity_2", valueOf(df.format(getDebtToEquityByQuarter(node, 2))));
        map.put("debtToEquity_3", valueOf(df.format(getDebtToEquityByQuarter(node, 3))));

        return map;
    }

    private double getPrice(final JsonNode node) {
        return node.get("price").asDouble();
    }

    private double getLatestEPS(final JsonNode node) {
        double latestEps = node.path("stats").get("latestEPS").asDouble();
        return latestEps == 0
                ? valueOf(df.format(getEPSByQuarter(node, 0)))
                : latestEps;
    }

    private double getDebtToEquityByQuarter(final JsonNode node, int quarter) {
        double liabilities;
        double currentLiabilities = getPropertyValueByQuarter(node, "financials", "totalLiabilities", quarter);
        double totalDebt = getPropertyValueByQuarter(node, "financials", "totalDebt", quarter);

        if (currentLiabilities > 0 && currentLiabilities < totalDebt) {
            liabilities = currentLiabilities;
        } else if (totalDebt != 0.0) {
            liabilities = totalDebt;
        } else {
            liabilities = 0.0;
        }

        return calculateDebtToEquity(
                liabilities,
                getPropertyValueByQuarter(node, "financials", "shareholderEquity", quarter));
    }

    private double getROEByQuarter(final JsonNode node, final int quarter) {
        return calculateROE(
                getPropertyValueByQuarter(node, "financials", "shareholderEquity", quarter),
                getPropertyValueByQuarter(node, "financials", "netIncome", quarter));
    }

    private double getEPSByQuarter(final JsonNode node, final int quarter) {
        return getPropertyValueByQuarter(node, "earnings", "actualEPS", quarter);
    }

    // TODO if exception caught -> don't update that value, keep the previous one
    private double getPropertyValueByQuarter(final JsonNode node, final String nodeName, final String property, final int quarter) {
        ArrayNode nodes;
        try {
            nodes = (ArrayNode) node.path(nodeName).get(nodeName);
            return nodes.get(quarter).get(property).asDouble();
        } catch (RuntimeException e) {
            throw new NodeNotFoundException("API data temporarily unavailable");
        }
    }

    private double calculatePERatio(final double price, final Double eps) {
        return eps > 0 ? price / eps : 0.0;
    }

    private double calculateROE(final Double shareholderEquity, final double netIncome) {
        return (shareholderEquity != null && shareholderEquity != 0) ? (netIncome / shareholderEquity) : 0.0;
    }

    private Double calculateDebtToEquity(final double totalLiabilities, final Double shareholderEquity) {
        return (shareholderEquity != null && shareholderEquity != 0) ? (totalLiabilities / shareholderEquity) : 0.0;
    }

    //TODO add method to verify the null/empty fields so user has a choice to add stock with missing data
    private double verify(final Double value) {
        if (value != null && value != 0 && !value.isNaN()) {
            return value;
        } else {
            log.error("Inform the user that some value is missing.");
            throw new ValueNotFoundException();
        }
    }

}
