package com.portfolio.stocks.client;

import com.portfolio.stocks.client.data.retriever.trend.calculator.DataRetrieverService;
import com.portfolio.stocks.client.data.retriever.trend.calculator.trend.calculator.TrendCalculator;
import com.portfolio.stocks.domain.Stock;
import com.portfolio.stocks.validators.IEXValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@RequiredArgsConstructor
@Component
public class IEXStockCreator {

    private final DataRetrieverService dataRetrieverService;
    private final IEXValidator validator;
    private final TrendCalculator trendCalculator;

    /**
     * Retrieves all data necessary to create a single stock object.
     *
     * @return Stock with all needed fields set up
     */
    public Stock createIEXStock(final String owner, final String portfolio, final String ticker) throws IOException {
        validator.validateIfAlreadyExists(owner, ticker);
        Stock stock = new Stock();

        HashMap<String, Object> statsMap =
                (HashMap<String, Object>) dataRetrieverService.retrieveStatsAndDescription(ticker);

        double roe_0 = (double) statsMap.get("roe_0");
        double roe_1 = (double) statsMap.get("roe_1");
        double roe_2 = (double) statsMap.get("roe_2");
        double roe_3 = (double) statsMap.get("roe_3");
        double actualEPS_0 = (double) statsMap.get("actualEPS_0");
        double actualEPS_1 = (double) statsMap.get("actualEPS_1");
        double actualEPS_2 = (double) statsMap.get("actualEPS_2");
        double actualEPS_3 = (double) statsMap.get("actualEPS_3");
        double debtToEquity_0 = (double) statsMap.get("debtToEquity_0");
        double debtToEquity_1 = (double) statsMap.get("debtToEquity_1");
        double debtToEquity_2 = (double) statsMap.get("debtToEquity_2");
        double debtToEquity_3 = (double) statsMap.get("debtToEquity_3");

        double trend_ROE = trendCalculator.calculateTrend(
                roe_0, roe_1, roe_2, roe_3);
        double trend_EPS = trendCalculator.calculateTrend(
                actualEPS_0, actualEPS_1, actualEPS_2, actualEPS_3);
        double trend_DebtToEquity = -trendCalculator.calculateTrend(
                debtToEquity_0, debtToEquity_1, debtToEquity_2, debtToEquity_3);

        stock.setTicker(ticker.toUpperCase());
        stock.setName((String) statsMap.get("companyName"));
        stock.setSector((String) statsMap.get("sector"));
        stock.setIndustry((String) statsMap.get("industry"));
        stock.setDescription((String) statsMap.get("description"));
        stock.setPrice((double) statsMap.get("price"));
        stock.setPriceToBook((double) statsMap.get("priceToBook"));
        stock.setLatestEPS((double) statsMap.get("latestEPS"));
        stock.setPeRatio((double) statsMap.get("peRatio"));
        stock.setReturnOnEquity((double) statsMap.get("returnOnEquity"));
        stock.setReturnOnAssets((double) statsMap.get("returnOnAssets"));
        stock.setDividendYield((double) statsMap.get("dividendYield"));
        stock.setTrend_ROE(trend_ROE);
        stock.setTrend_EPS(trend_EPS);
        stock.setTrend_DebtToEquity(trend_DebtToEquity);
        stock.setRoe_0(roe_0);
        stock.setRoe_1(roe_1);
        stock.setRoe_2(roe_2);
        stock.setRoe_3(roe_3);
        stock.setActualEPS_0(actualEPS_0);
        stock.setActualEPS_1(actualEPS_1);
        stock.setActualEPS_2(actualEPS_2);
        stock.setActualEPS_3(actualEPS_3);
        stock.setDebtToEquity_0(debtToEquity_0);
        stock.setDebtToEquity_1(debtToEquity_1);
        stock.setDebtToEquity_2(debtToEquity_2);
        stock.setDebtToEquity_3(debtToEquity_3);
        stock.setPortfolio(portfolio);
        stock.setOwner(owner);
        return stock;
    }
}