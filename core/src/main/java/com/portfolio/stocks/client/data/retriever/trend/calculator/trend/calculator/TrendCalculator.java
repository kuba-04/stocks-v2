package com.portfolio.stocks.client.data.retriever.trend.calculator.trend.calculator;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class TrendCalculator {

    private static DecimalFormat df = new DecimalFormat(".####");

    public double calculateTrend(final Double... vals) {
        SimpleRegression regression = new SimpleRegression();
        for (int i = vals.length - 1; i >= 0; i--) {
            if (vals[i] != null) {
                regression.addData(vals.length - i, vals[i]);
            }
        }

        Double slope = regression.getSlope();
        if (!slope.isNaN() && !slope.isInfinite()) {
            return Double.valueOf(df.format(slope));
        }
        return 0.0;
    }
}
