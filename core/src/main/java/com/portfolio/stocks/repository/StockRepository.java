package com.portfolio.stocks.repository;

import com.portfolio.stocks.domain.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface StockRepository extends MongoRepository<Stock, Long> {

    List<Stock> findAllByOwner(String owner);

    List<Stock> findAllByOwnerAndPortfolio(String owner, String portfolio);

    @Query(value = "{$and : [ {owner : ?0}, {portfolio : ?1} ] }", sort = "{sortOrder: -1}")
    List<Stock> findAllByOwnerAndPortfolioSorted(String owner, String portfolio);

    Optional<Stock> findByOwnerAndTicker(String owner, String ticker);

    void deleteByOwnerAndTicker(String owner, String ticker);

    void deleteAllByOwnerAndPortfolio(String owner, String portfolio);

}
