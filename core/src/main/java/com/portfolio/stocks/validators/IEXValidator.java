package com.portfolio.stocks.validators;

import com.portfolio.stocks.exceptions.EquityAlreadyExistsException;
import com.portfolio.stocks.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class IEXValidator {

    private final StockRepository stockRepository;

    public void validateIfAlreadyExists(final String owner, final String ticker) {
        if (stockRepository.findByOwnerAndTicker(owner, ticker.toUpperCase()).isPresent()) {
            throw new EquityAlreadyExistsException(ticker);
        }
    }

}
