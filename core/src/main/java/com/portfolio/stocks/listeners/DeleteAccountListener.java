package com.portfolio.stocks.listeners;

import com.portfolio.stocks.registration.verification.events.OnDeleteUserEvent;
import com.portfolio.stocks.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeleteAccountListener implements ApplicationListener<OnDeleteUserEvent> {

    private final StockRepository stockRepository;

    @Override
    public void onApplicationEvent(final OnDeleteUserEvent event) {
        deleteUserReferences(event.getUsername());
    }

    private void deleteUserReferences(final String username) {
        stockRepository.findAllByOwner(username);
        log.info("deleted references to user: {}", username);
    }

}
