[![Build Status](https://travis-ci.com/kuba-04/stocks-v2.svg?branch=master)](https://travis-ci.com/kuba-04/stocks-v2)

## Stocks Application

Stocks application allows you to keep your stocks in segregated portfolios. The main feature is to sort your favourites according to fundamental parameters.
Sorting in separate portfolios lets you choose the best ones from particular category like sector/industry or you can choose the bests from an ETF or individual investors picks.

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Installing

- Download back-end the code and build:
```
./gradlew build
```
then start the app from jar or IDE

Spring Boot application will be running on port 8090

To play around on your local machine and see the results you need to also download its [front-end](https://github.com/kuba-04/stocks-v2-ng) part 


- Download front-end code and run from the project directory:

```
ng serve
```

Angular application will be running on port 4200 by default


### Acknowledgments

Data provided for free by [IEX](https://iextrading.com/developer). View IEX’s [Terms of Use](https://iextrading.com/api-exhibit-a/).
